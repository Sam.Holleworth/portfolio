import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.io.*;

/**
 * A single measurement of NO<sub>2</sub> level.
 *
 * <p>Created for COMP1721 Coursework 1.</p>
 *
 * @author SAM HOLLEWORTH
 */
public class Measurement {
  // Use this when parsing measurement time
  private static DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
  private static DateTimeFormatter toStringFORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
  private static DateTimeFormatter dateFORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");

//Fields//////////////////////////////////////////////////////////////////////////////////////////////

  private LocalDateTime time;
  private LocalDate date;
  private int level;
  private String levelString;

/////////////////////////////////////////////////////////////////////////////////////////////////////////

  public Measurement(String record) {

///Split a single record line at each comma and place each part in an array
    String recordArray[] = record.split(",");



    if(recordArray.length > 3 || recordArray.length < 2){
      throw new DataException("Invalid Data");//throw exception if record is wrong size
    }
    else {

      String temptime = recordArray[0] + ' ' + recordArray[1];//combine date and time in string

      time = LocalDateTime.parse(temptime, FORMAT);//format date and time

      date = LocalDate.parse(recordArray[0], dateFORMAT);// format just the date

      if(recordArray.length == 3){
        level = Integer.parseInt(recordArray[2]);//change level to an int if there is one
      }
      else {
        level = -1;//chande level to -1 if nothing is found on record
      }
    }
  }

  public LocalDateTime getTime() {
    return time;
  }

  public int getLevel() {
    return level;
  }

  public LocalDate getDate() {
    return date;
  }

  public String toString() {
    String timeString = time.format(toStringFORMAT);//specific format for this instruction
    if (level == -1){
      levelString = "no data";
    }
    else{
      levelString = Integer.toString(level) + " \u00b5g/m\u00b3";//turn int level into a string
    }
    String measurementString = timeString + ", " + levelString ;// combine all of them together
    return measurementString;
  }
}
