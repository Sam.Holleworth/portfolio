import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.*;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.Group;

public class pieChart extends Application {


  PollutionDataset dataset = new PollutionDataset();



  dataset.readCSV(file);


  @Override
  public void start(Stage primaryStage){
    Scene scene = new Scene(new Group());
    primaryStage.setTitle("Quarterly NO2 Levels");
    primaryStage.setWidth(500);
    primaryStage.setHeight(500);

    ObservableList<PieChart.Data> pieChartData =
        FXCollections.observableArrayList(
        new PieChart.Data("1st Quarter", 20),
        new PieChart.Data("2nd Quarter", 35),
        new PieChart.Data("3rd Quarter", 27),
        new PieChart.Data("4th Quarter", 15)
        );
    final PieChart chart = new PieChart(pieChartData);
    chart.setTitle("Quarterly NO2 Levels");

    ((Group) scene.getRoot()).getChildren().add(chart);
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}

public static void main(String[] args) {
    Scanner input = new Scanner();
    System.out.println("Please enter the name of the file you wish to chart: ");
    String file = input.nextLine();
}
