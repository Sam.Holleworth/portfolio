import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.io.*;


/**
 * A sequence of air pollution measurements.
 *
 * <p>Created for COMP1721 Coursework 1.</p>
 *
 * @author SAM HOLLEWORTH
 */
public class PollutionDataset {

//FIELDS/////////////////////////////////////////////////////////////////////////
  private List<Measurement> dataset;
  private int datasetLevel;
  private Measurement highestNO2;
  private int highestNO2Level;
  private int count;
  private Measurement minLevelMeasurement;
  //////////////////////////////////////////////////////////////////////////////


  public PollutionDataset() {
    dataset = new ArrayList<Measurement>();//initialise dataset list
  }

  public void readCSV(String filename) throws FileNotFoundException {

    dataset.clear();//clear list ready for new file

    Scanner input = new Scanner(new File(filename));

    input.nextLine();//skip header line in file

//read through each line, turn them into measurements, and add them to the list
    while (input.hasNextLine()) {
      String nextMeasurement = input.nextLine();
      Measurement tempset = new Measurement(nextMeasurement);

      dataset.add(tempset);
    }

    input.close();
  }

  public void add(Measurement m) {
    dataset.add(m);//adds measurement to end of list
  }

  public int size() {
    return dataset.size();
  }

  public Measurement get(int index) {
    if (dataset.isEmpty()){
      throw new DataException("No items in dataset.");
    }
    return dataset.get(index);
  }

//finds measurment with highest NO2 level and returns it
  public Measurement maxLevel() {
    highestNO2Level = 0;
    if(dataset.isEmpty()){
      throw new DataException("No items in dataset.");
    }
    else{
      for(int i = 0; i < dataset.size(); i++){

        datasetLevel = dataset.get(i).getLevel();


        if (datasetLevel > highestNO2Level){
          highestNO2Level = datasetLevel;
          highestNO2 = dataset.get(i);
        }
      }
    }
    return highestNO2;
  }


//finds measurement with lowest NO2 level and returns it
  public Measurement minLevel() {

    if(dataset.isEmpty()){
      throw new DataException("No items in dataset.");
    }
    else {

      datasetLevel = 10000;

      for(int i = 0; i < dataset.size(); i++){
        if(dataset.get(i).getLevel() != -1){
          if(dataset.get(i).getLevel() < datasetLevel){
            datasetLevel = dataset.get(i).getLevel();
            minLevelMeasurement = dataset.get(i);
          }
        }
      }
      return minLevelMeasurement;

    }
  }

//creates a mean of all the valid NO2 levels and returns it
  public double meanLevel() {
    datasetLevel = 0;

    if(dataset.isEmpty()){
      throw new DataException("No items in dataset.");
    }
    else{
      for(int i = 0; i < dataset.size(); i++){
        if(dataset.get(i).getLevel() != -1){
          datasetLevel += dataset.get(i).getLevel();
          count++;
        }
      }
      int meanLevel = datasetLevel/count;
      return meanLevel;
    }
  }

//if the annual limit for NO2 breaches is reached, returns date and time it was breached
  public LocalDate dayRulesBreached() {
    int hourlyLevel = 0;
    count = 0;
    int timesBreached = 0;
    LocalDate dayBreached = null;

    if(dataset.isEmpty()){
      throw new DataException("No items in dataset.");
    }
    else {
      for(int i = 0; i < dataset.size(); i++){
        hourlyLevel += dataset.get(i).getLevel();
        count++;
        if(count == 4){
          count = 0;

          if(hourlyLevel > 200){
            timesBreached++;

            if(timesBreached == 18){
              dayBreached =  dataset.get(i).getDate();
            }

          }

          hourlyLevel = 0;
        }

      }
      return dayBreached;
    }
  }

}
