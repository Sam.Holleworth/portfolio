import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Pollution {

  public static void main(String[] args) {

    System.out.println("Please enter the name of the file you wish to analyse: ");


    PollutionDataset inputDataSet = new PollutionDataset();

    if(args[0] != null){
      try{
        inputDataSet.readCSV(args[0]);
      }
      catch (Exception e) {
      System.out.println("Please enter your filename.");
      System.exit(0);
    }
    }

    System.out.println(inputDataSet.size() + " records processed.");
    System.out.println("Max " + inputDataSet.maxLevel());
    System.out.println("Min " + inputDataSet.minLevel());
    System.out.println("Mean " + inputDataSet.meanLevel());

    if(inputDataSet.dayRulesBreached() == null){
      System.out.println("The EU rules were not breached");
    }
    else {
      System.out.println("The EU rules were breached on " + inputDataSet.dayRulesBreached() + ".");
    }
  }

}
