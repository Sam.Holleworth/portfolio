import java.util.Scanner;
import java.io.*;

public class BankAccount
{

  private int id;
  private String name;
  protected int balance;

  public BankAccount(int i, String nam, int bal)
  {
    id = i;
    name = nam;
    balance = bal;
  }

  //Return all attributes of BankAccount
  public int getId(){return id;}
  public String getName(){return name;}
  public int getBalance(){return balance;}

  //Add money to account
  public void deposit(int amount)
  {
    if(amount <= 0)
    {
      throw new IllegalArgumentException("Amount deposited must be over 0.");
    }

    balance = balance + amount;
  }

  //Withdraw money from account
  public void withdraw(int amount)
  {

    if(amount <= 0 || amount > balance)
    {
      throw new IllegalArgumentException("Amount withdrawn unacceptable.");
    }
    balance = balance - amount;

  }

  @Override
  public String toString()
  {
    return String.format("Customer ID: %d \nCustomer Name: %s \nCustomer Balance: %d", id, name, balance);
  }
}
