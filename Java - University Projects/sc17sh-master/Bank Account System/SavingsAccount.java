import java.util.Scanner;
import java.io.*;

public class SavingsAccount extends BankAccount
{
  //Holds Interest Rate
  private int interestRate;

  public SavingsAccount(int id, String name, int bal, int rate)
  {
    super(id, name, bal);

    this.interestRate = rate;
  }

  //Add exception for account being over £10,000
  @Override
  public void deposit(int amount)
  {
    if(super.getBalance() + amount > 10000)
    {
      throw new IllegalArgumentException("Balance must remain under £10,000.");
    }
    super.deposit(amount);
  }

  //Set interest rate
  public void setInterest(int rate)
  {
    interestRate = rate;
  }

  //Return interest rate
  public int getInterest()
  {
    return interestRate;
  }

  //Add interest as % of current balance
  public void applyInterest()
  {
    super.deposit((super.getBalance()/100) * interestRate);
  }

  //Override to include savings account type and interest rate as string
  @Override
  public String toString()
  {
    //Create string with super classes toString
    String bankAccountString = super.toString();
    //Create string with % sign
    String interestString = Integer.toString(interestRate) + "%";

    return String.format("Account Type: Savings Account\n" + bankAccountString + "\nInterest Rate: %s", interestString);

  }

}
