import java.io.*;

public class StudentAccount extends BankAccount
{

  public StudentAccount(int id, String name, int bal)
  {
    super(id, name, bal);
  }

  //Override withdraw to add overdraft 
  @Override
  public void withdraw(int amount)
  {
    if(balance - amount < -250 || amount <= 0)
    {
      throw new IllegalArgumentException("Overdraft only allows £250.");
    }
    balance -= amount;
  }

  public String toString()
  {
    String bankAccountString = super.toString();

    return String.format("Account Type: Student Account\n" + bankAccountString);
  }
}
