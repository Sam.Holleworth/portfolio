import java.util.*;
import java.io.*;

public class AccountTest
{
  public static void main(String[] args)
  {

    //Create list to hold account objects
    ArrayList<BankAccount> accounts = new ArrayList<>();

    //Create account objects
    StudentAccount account1 = new StudentAccount(1, "Sam", 100);
    StudentAccount account2 = new StudentAccount(2, "June", 200);
    SavingsAccount account3 = new SavingsAccount(3, "George", 300, 5);
    SavingsAccount account4 = new SavingsAccount(4, "Justin", 400, 10);

    //Add objects to list
    accounts.add(account1);
    accounts.add(account2);
    accounts.add(account3);
    accounts.add(account4);

    //Print out info on all objects
    for(int i = 0; i < accounts.size(); ++i)
    {
      System.out.println(accounts.get(i));
    }

    System.out.println();

    //Withdraw money from all accounts
    for(int i = 0; i < accounts.size(); ++i)
    {
      accounts.get(i).withdraw(200);
    }

    System.out.println();

    //Print out new account info
    for(int i = 0; i < accounts.size(); ++i)
    {
      System.out.println(accounts.get(i));
    }

  }
}
