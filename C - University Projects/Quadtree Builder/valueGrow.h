#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "buildTree.h"
#include "writeTree.h"
#include "treeStructure.h"
#include "valueTree.h"
#include "stdbool.h"

int valueGrow(Node* node);

int trackGrowth(Node* node, int count);
