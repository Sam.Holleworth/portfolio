#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "buildTree.h"
#include "writeTree.h"
#include "treeStructure.h"
#include "valueTree.h"
#include "stdbool.h"
#include "valueGrow.h"

static int count = 0;

//checks indicator and grows nodes if false

int valueGrow(Node* node) {

  int i;

  if( node->child[0] == NULL ){

    if ( indicator( node, 0.2, 2 ) == false) {
      makeChildren( node );
      count++;
    }
  }
  else {
    for ( i=0; i<4; ++i ) {
      valueGrow( node->child[i] );
    }
  }

  trackGrowth(node, count);
  printf("%i", count);

  return count;
}

//tracks how many nodes have been grown by valueGrow, if some then runs it again

int trackGrowth(Node* node, int counter) {

  if(count > 0){
    count = 0;
    valueGrow( node );
  }

}
