#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "buildTree.h"
#include "writeTree.h"
#include "treeStructure.h"
#include "destroyTree.h"
#include "growTree.h"
#include "valueGrow.h"

void task1();
void task2();
void task3();
void task4();
