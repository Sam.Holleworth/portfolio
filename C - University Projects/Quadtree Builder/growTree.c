#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "treeStructure.h"

//grows all leaf nodes the amount of times specified by times

void growTree(Node *node, int times) {

    int i;
    int j;

for(j = 0; j<times ; j++){

    if( node->child[0] == NULL )
      makeChildren( node );
    else {
      for ( i=0; i<4; ++i ) {
        growTree( node->child[i], 1 );
      }
    }

  }
    return;

}
