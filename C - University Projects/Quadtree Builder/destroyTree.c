#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "writeTree.h"
#include "treeStructure.h"

// recursively destroy the leaf nodes

void destroyTree(Node *node) {
  int i;

  if( node->child[0] == NULL )
    free(node);
  else {
    for ( i=0; i<4; ++i ) {
      destroyTree( node->child[i] );
    }
  free(node);
  }
  return;
}
