#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "buildTree.h"
#include "writeTree.h"
#include "treeStructure.h"
#include "destroyTree.h"
#include "growTree.h"
#include "valueGrow.h"

//destroyTree test

void task1() {

  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren( head );

  int i;

  for (i = 0; i < 4; i++ ){
    makeChildren( head->child[i] );
  }
  makeChildren ( head->child[2]->child[3] );

  writeTree( head );
  destroyTree( head );


}

//growTree test

void task2() {

  Node* head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren( head );

  growTree( head, 1 );

  writeTree( head );
  destroyTree( head );


}

//memory usage test

void task3() {

  Node* head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren( head );

  growTree( head, 2 );
  makeChildren ( head->child[2]->child[0]->child[0] );
  makeChildren ( head->child[2]->child[1]->child[0] );
  makeChildren ( head->child[0]->child[1]->child[3] );
  growTree( head, 1);

  writeTree( head );
  destroyTree( head );


}

//valueGrow test

void task4() {

  Node* head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren( head );
  growTree(head, 1);


  valueGrow( head );


  writeTree( head );
  destroyTree( head );

}
