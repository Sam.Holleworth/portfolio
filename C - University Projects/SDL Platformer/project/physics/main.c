#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"

#include "LTexture.h"
#include "LTimer.h"
#include "Dot.h"
#include "LItem.h"
#include "LEnemy.h"
#include "LPortal.h"

#include "concat.h"



/////////////////////////////////////////////////////////////////////////////

bool init()
{
	//Set nulls
	gWindow = NULL;
	gRenderer = NULL;
	gFont = NULL;

	isFalling = false;

	score = 0;

	enemyLeft = false;

	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Squaremageddon", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				int imgFlags = IMG_INIT_PNG;
				if(!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}

				if (TTF_Init() == -1)
				{
					printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool loadMedia()
{
	bool success = true;

	//Open the font
	gDotTexture = loadFromFile("../images/dot.bmp");
	if(gDotTexture.mTexture == NULL)
	{
		printf("Unable to load dot texture!");
	}

	gMenuTexture = loadFromFile("../images/Start.png");
	if(gMenuTexture.mTexture == NULL)
	{
		printf("Unable to load start menu texture");
	}

	//Open the font
	gFont = TTF_OpenFont("../images/lazy.ttf", 28);
	if(gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}

	else
	{
		//Render lose text
		SDL_Color textColor = {255, 255, 255, 255};

		loseText = loadFromRenderedText("You Lose!", textColor);

    restartText = loadFromRenderedText("Press R to TRY AGAIN", textColor);

		//Render win text
		textColor.r = 0; textColor.g = 0; textColor.b = 0;

		winText = loadFromRenderedText("YOU WIN!", textColor);
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool checkCollision(SDL_Rect a, SDL_Rect b)
{
	//The sides of the rectangles
	int leftA;
	int leftB;
	int rightA;
	int rightB;
	int topA;
	int topB;
	int bottomA;
	int bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;
	bool success = false;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//If any of the sides from A are outside of B
	if(bottomA <= topB)
	{
		success = false;
	}

	else if(topA >= bottomB)
	{
		success = false;
	}

	else if(rightA <= leftB)
	{
		success = false;
	}

	else if(leftA >= rightB)
	{
		success = false;
	}

	else
	{
		success = true;
	}

	//If none of the sides from A are outside B
return success;
}

////////////////////////////////////////////////////////////////////////////////
//Check if cube is on solid ground to enable jump
Dot canJump(Dot dot, SDL_Rect walls[])
{
	//Find sides of cube
	int bottomA = dot.mCollider.y + dot.mCollider.h;
	int rightA = dot.mCollider.x + dot.mCollider.w;
	int leftA = dot.mCollider.x;

	//Increment through walls
	int i;
	for(i = 0; i < NUM_OF_WALLS; i++)
	{
		//Find top of walls
		int topB = walls[i].y;
		//If cube is on a platform enable jump
		if(bottomA == topB && rightA > walls[i].x && leftA < walls[i].x + walls[i].w)
		{
			//Set jump height from current platform
			dot.jumpY = dot.mPosY - JUMP_HEIGHT;
			//dot.mVelY = 0;
			isFalling = false;

		}
		//Set jump if cube touches floor
		else if(bottomA == SCREEN_HEIGHT)
		{
			running = false;
			//Set for level reset
			resetted = false;
		}
	}
	return dot;
}

///////////////////////////////////////////////////////////////////////////
//Check if cube hit's ceiling and disable jump if it does

Dot checkCeiling(Dot dot, SDL_Rect walls[])
{
	int topA = dot.mCollider.y;
	int rightA = dot.mCollider.x + dot.mCollider.w;
	int leftA = dot.mCollider.x;

	int i;
	for(i = 0; i < NUM_OF_WALLS; i++)
	{
		int bottomB = walls[i].y + walls[i].h;

		if (topA == bottomB && rightA >= walls[i].x && leftA <= walls[i].x + walls[i].w)
		{
			dot.mVelY = 0;
			isFalling = true;
		}
	}
	return dot;
}

/////////////////////////////////////////////////////////////////////////////

void makeWalls()
{
	walls[0].x = (SCREEN_WIDTH - 450)/2;
	walls[0].y = 400;
	walls[0].w = 450;
	walls[0].h = 20;

	walls[1].x = (SCREEN_WIDTH - 100)/4;
	walls[1].y = 300;
	walls[1].w = 100;
	walls[1].h = 20;

	walls[2].x = ((SCREEN_WIDTH - 100)/4)*3;
	walls[2].y = 200;
	walls[2].w = 100;
	walls[2].h = 20;

	walls[3].x = (SCREEN_WIDTH - 100)/4;
	walls[3].y = 100;
	walls[3].w = 300;
	walls[3].h = 20;
}

///////////////////////////////////////////////////////////////////////////////

void clean()
{
	release(gDotTexture);
	release(gMenuTexture);

	release(loseText);
	release(pointsTexture);
	release(restartText);


	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

//////////////////////////////////////////////////////////////////////////////

Dot reset(Dot dot)
{
	dot.mPosX = (SCREEN_WIDTH - DOT_WIDTH)/2;
	dot.mPosY = 380;
	dot.mGrav = DOT_GRAV;
	dot.jumpY = 0;
	dot.mCollider.y = dot.mPosY;
	dot.mCollider.x = dot.mPosX;
	dot.mCollider.w = DOT_WIDTH;
	dot.mCollider.h = DOT_HEIGHT;
	dot.mVelX = 0;
	dot.mVelY = 0;

	isFalling = true;

	grow = true;

	makeItems(items);
	makeEnemies(enemies);
	portal = makePortal(portal);

	score = 0;

	return dot;
}

/////////////////////////////////////////////////////////////////////////////

void setForegroundColour()
{
	if(score == 0)
	{
		SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 0xFF);
	}
	else if(score == 1)
	{
		SDL_SetRenderDrawColor(gRenderer, 180, 180, 180, 0xFF);
	}
	else if(score == 2)
	{
		SDL_SetRenderDrawColor(gRenderer, 100, 100, 100, 0xFF);
	}
	else if(score == 3)
	{
		SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 0xFF);
	}
}

//////////////////////////////////////////////////////////////////////////

void setBackgroundColour()
{
	if(score == 0)
	{
		SDL_SetRenderDrawColor(gRenderer, 50, 50, 50, 0xFF);
	}
	else if(score == 1)
	{
		SDL_SetRenderDrawColor(gRenderer, 100, 100, 100, 0xFF);
	}
	else if(score == 2)
	{
		SDL_SetRenderDrawColor(gRenderer, 180, 180, 180, 0xFF);
	}
	else if(score == 3)
	{
		SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 0xFF);
	}
}

//////////////////////////////////////////////////////////////////////////////

SDL_Color setTextColour()
{
	SDL_Color textColor;
	if(score == 0)
	{
		textColor.r = 255; textColor.g = 255; textColor.b = 255;
	}
	else if(score == 1)
	{
		textColor.r = 180; textColor.g = 180; textColor.b = 180;
	}
	else if(score == 2)
	{
		textColor.r = 100; textColor.g = 100; textColor.b = 100;
	}
	else if(score == 3)
	{
		textColor.r = 50; textColor.g = 50; textColor.b = 50;
	}
	return textColor;
}

//////////////////////////////////////////////////////////////////////////////

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{

		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			running = false;

			//Show start menu
			launched = true;

			up = true;

			//The dot that will be moving around the screen
			Dot dot;
			dot.mPosX = 0;
			dot.mPosY = 0;
			dot.mGrav = DOT_GRAV;
			dot.jumpY = 0;
			dot.mCollider.w = DOT_WIDTH;
			dot.mCollider.h = DOT_HEIGHT;
			dot.mVelX = 0;
			dot.mVelY = 0;

			//Set dimensions and posiitons of platforms
			makeWalls();

			//Set dimensions and positions of items
			makeItems(items);

			//Set dimensions and positions of enemies
			makeEnemies(enemies);

			//Set dimensions and positions of portal
			portal = makePortal(portal);

			//WIll hold string made from score
			char *pointsText;


			//While application is running
			while( !quit )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}
					if(e.type == SDL_KEYDOWN)
					{
						if (e.key.keysym.sym == SDLK_r) {
							running = true;
							won = false;
						}
						if (e.key.keysym.sym == SDLK_SPACE && launched == true)
						{
							running = true;
							launched = false;
						}
					}
					//Handle input for the dot
					if(running)
					{
						dot = handleEvent(dot, e);
					}
				}

				//Show start menu
				if(!running && launched)
				{
					//Clear screen
	        SDL_SetRenderDrawColor(gRenderer, 50, 50, 50, 0xFF);
	        SDL_RenderClear( gRenderer );

					//Render menu texture
					render(gMenuTexture, 0, 0, NULL);

					SDL_RenderPresent(gRenderer);
				}

				//Display win menu
				else if(!running && won)
				{

					//If level has not been reset
					if(resetted == false)
					{
						dot = reset(dot);
						resetted = true;
					}

					//Clear screen
	        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	        SDL_RenderClear( gRenderer );

					render(winText, (SCREEN_WIDTH - getWidth(winText))/2, (SCREEN_HEIGHT - getHeight(winText))/2, NULL);

					SDL_RenderPresent(gRenderer);
				}

				//Display restart menu
				else if(!running && !launched && !won)
				{

					int savedScore;

					//If level has not been reset
					if(resetted == false)
					{
						//Save score before reset
						savedScore = score;
						dot = reset(dot);
						resetted = true;
					}

					//Array for holding score integer
					char points[5];

					//Changes score integer to string
	        sprintf(points, "%d", savedScore);

					//Joins strings together
					pointsText = concat("Points Collected: ", points);

					//Set text colour
	        SDL_Color textColor = {255, 255, 255, 255};
					//Make fina texture to be displayed
	        pointsTexture = loadFromRenderedText(pointsText, textColor);

					//Clear screen
	        SDL_SetRenderDrawColor(gRenderer, 50, 50, 50, 0xFF);
	        SDL_RenderClear( gRenderer );

					//Render text to screen
	        render(loseText, (SCREEN_WIDTH - getWidth(loseText))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 - 50 , NULL);
	        render(pointsTexture, (SCREEN_WIDTH - getWidth(pointsTexture))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 , NULL);
	        render(restartText, (SCREEN_WIDTH - getWidth(restartText))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 + 50 , NULL);

					SDL_RenderPresent(gRenderer);
				}

				//Run the game
				else if(running && !launched)
				{
					//Clear screen

					setBackgroundColour();
					SDL_RenderClear( gRenderer );

					//Render platforms to screen
					int i;
					SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
					for(i = 0; i < NUM_OF_WALLS; i++)
					{
						SDL_RenderFillRect(gRenderer, &walls[i]);
					}

					//Render character to screen
					setForegroundColour();
					SDL_RenderFillRect(gRenderer, &dot.mCollider);

					//Array for holding score integer
					char points[5];
					//Changes score integer to string
	        sprintf(points, "%d", score);
					pointsText = concat("Score: ", points);
					//Set text colour
	        SDL_Color textColor = setTextColour();
					//Make fina texture to be displayed
	        scoreTexture = loadFromRenderedText(pointsText, textColor);
					//Render live score to screen
					render(scoreTexture, 10, 10, NULL);

					//Render items to screen
					SDL_SetRenderDrawColor(gRenderer, 255, 200, 0, 0xFF);
					for(i = 0; i < NUM_OF_ITEMS; i++)
					{
						SDL_RenderFillRect(gRenderer, &items[i].iCollider);
					}

					//Render enemies to screen
					SDL_SetRenderDrawColor(gRenderer, 215, 89, 115, 255);
					for(i = 0; i < NUM_OF_ENEMIES; i++)
					{
						SDL_RenderFillRect(gRenderer, &enemies[i].iCollider);
					}

					//Render portal to screen
					SDL_SetRenderDrawColor(gRenderer, 78, 233, 250, 255);
					SDL_RenderFillRect(gRenderer, &portal.iCollider);

					//Move the dot
					dot = canJump(dot, walls);
					dot = checkCeiling(dot, walls);
					dot = gravity(dot, walls);
					dot = move(dot, walls);


					animateItem(items);
					collect(dot, items);

					animateEnemy(enemies, walls);
					contact(dot, enemies);

					if(score == FULL_SCORE)
					{
						portal = animatePortal(portal);
					}
					enter(dot, portal);
					//Render dot
					//renderDot(dot);

	        //Update screen
	        SDL_RenderPresent(gRenderer);
				}
			}
		}
		//Free resources and close SDL
		clean();
	}
	return 0;
}
