#include "main.h"
#include "LTexture.h"

Dot handleEvent(Dot dot, SDL_Event e)
{
  //If key was pressed
  if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
  {
    //Adjust the velocity
    switch(e.key.keysym.sym)
    {
      case SDLK_UP:
      if(!isFalling)
      {
        dot.mVelY -= DOT_JUMPING_VEL;
      };
      break;
      case SDLK_LEFT: dot.mVelX -= DOT_VEL; break;
      case SDLK_RIGHT: dot.mVelX += DOT_VEL; break;
    }
  }
  else if(e.type == SDL_KEYUP && e.key.repeat == 0)
  {
    //Adjust velocity
    switch(e.key.keysym.sym)
    {
      case SDLK_UP:
      if(!isFalling)
      {
        dot.mVelY = 0;
        isFalling = true;
      };
      break;
      case SDLK_LEFT: dot.mVelX += DOT_VEL; break;
      case SDLK_RIGHT: dot.mVelX -= DOT_VEL; break;
    }
  }
  return dot;
}

Dot move(Dot dot, SDL_Rect walls[])
{
 //Move dot left or right
 dot.mPosX += dot.mVelX;
 dot.mCollider.x = dot.mPosX;

  int i;
  //If dot moved too far
  for(i = 0; i < NUM_OF_WALLS; i++)
  {
    if( dot.mPosX < 0 || (dot.mPosX + DOT_WIDTH > SCREEN_WIDTH) || checkCollision(dot.mCollider, walls[i]))
    {
      //Move back
      dot.mPosX -= dot.mVelX;
      dot.mCollider.x = dot.mPosX;
    }
  }

  //Check collision on ceiling to stop jump
  //checkCeiling(dot, walls);

  //Move dot up or down
  dot.mPosY += dot.mVelY;
  dot.mCollider.y = dot.mPosY;

   //If dot moved too far
   for(i = 0; i < NUM_OF_WALLS; i ++)
   {
     if( (dot.mPosY + DOT_HEIGHT > SCREEN_HEIGHT)  || checkCollision(dot.mCollider, walls[i]))
     {
       //Move back
       dot.mPosY -= dot.mVelY;
       dot.mCollider.y = dot.mPosY;
     }
   }


  return dot;
}

Dot gravity(Dot dot, SDL_Rect walls[])
{

  //Gravity pulls box down at 5 frames per second
  dot.mPosY += dot.mGrav;
  dot.mCollider.y = dot.mPosY;


  int i;
  for(i = 0; i < NUM_OF_WALLS; i ++)
  {
    if ((dot.mPosY + DOT_HEIGHT > SCREEN_HEIGHT)  || checkCollision(dot.mCollider, walls[i]))
    {
      //Move back
      dot.mPosY -= dot.mGrav;
      dot.mCollider.y = dot.mPosY;
    }
  }

  //Box falls when it hits the peak of the jump
  if(dot.mCollider.y <= dot.jumpY || dot.mPosY < 0 )
  {
    dot.mVelY = 0;
    printf("Peak Fall:%i\n", dot.mVelY);
    isFalling = true;
  }

  return dot;
}

void renderDot(Dot dot)
{
  //Show the dot
  render(gDotTexture, dot.mPosX, dot.mPosY, NULL);
}
