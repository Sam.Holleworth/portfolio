LTimer lTimerConstructor();
LTimer start(LTimer ltimer);
LTimer stop(LTimer ltimer);
LTimer pause(LTimer ltimer);
LTimer unpause(LTimer ltimer);
Uint32 getTicks();
bool isStarted(LTimer ltimer);
bool isPaused(LTimer ltimer);
