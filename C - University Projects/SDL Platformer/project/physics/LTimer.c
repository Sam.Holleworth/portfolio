#include "main.h"
#include "LTimer.h"

//Various clock actions
LTimer lTimerConstructor()
{
  LTimer ltimer;
  //Initialize the variables
  ltimer.mStartTicks = 0;
  ltimer.mPausedTicks = 0;

  ltimer.mPaused = false;
  ltimer.mStarted = false;

  return ltimer;
}

LTimer start(LTimer ltimer)
{
  //Start the timer
  ltimer.mStarted = true;

  //Unpause the timer
  ltimer.mPaused = false;

  //Get the current clock time
  ltimer.mStartTicks = SDL_GetTicks();
  ltimer.mPausedTicks = 0;

  return ltimer;
}

LTimer stop(LTimer ltimer)
{
  //Stop the time
  ltimer.mStarted = false;

  //Unpause the time
  ltimer.mPaused = false;

  //Clear tick variables
  ltimer.mStartTicks = 0;
  ltimer.mPausedTicks = 0;

  return ltimer;
}

LTimer pause(LTimer ltimer)
{

  //if the timer is running and isn't already paused
  if(ltimer.mStarted && !ltimer.mPaused)
  {
    //Pause the time
    ltimer.mPaused = true;

    //Calculate the paused ticks
    ltimer.mPausedTicks = SDL_GetTicks() - ltimer.mStartTicks;
    ltimer.mStartTicks = 0;
  }

  return ltimer;
}

LTimer unpause(LTimer ltimer)
{
  //If the tmer is running and paused
  if(ltimer.mStarted && ltimer.mPaused)
  {
    //Unpause the timer
    ltimer.mPaused = false;

    //Reset the starting ticks
    ltimer.mStartTicks = SDL_GetTicks() - ltimer.mPausedTicks;

    //Reset the paused ticks
    ltimer.mPausedTicks = 0;
  }
  return ltimer;
}

//Gets the timer's time
Uint32 getTicks(LTimer ltimer)
{
  //The actual timer time
  Uint32 tickTime = 0;

  //If the timer is running
  if(ltimer.mStarted)
  {
    if(ltimer.mPaused)
    {
      //Return the number of ticks when the timer was paused
      tickTime = ltimer.mPausedTicks;
    }
    else
    {
      //Return the current time minus the start time
      tickTime = SDL_GetTicks() - ltimer.mStartTicks;
      //printf("%d\n", tickTime);
    }
  }
  printf("%d\n", tickTime);
  return tickTime;
}

//Checks the status of the timer
bool isStarted(LTimer ltimer)
{
  return ltimer.mStarted;
}
bool isPaused(LTimer ltimer)
{
  return ltimer.mPaused;
}
