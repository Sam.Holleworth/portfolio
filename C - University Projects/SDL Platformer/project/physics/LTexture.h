
void constructLTexture(LTexture ltexture, SDL_Texture* t, int w, int h);
void deallocate(LTexture ltexture);
LTexture loadFromFile( char path[]);
#ifdef _SDL_TTF_H
LTexture loadFromRenderedText(char textureText[], SDL_Color textColor);
#endif
void release(LTexture ltexture);
void setColour(LTexture ltexture, Uint8 red, Uint8 green, Uint8 blue);
void setBlendMode(LTexture ltexture, SDL_BlendMode blending);
void setAlpha(LTexture ltexture, Uint8 alpha);
void render(LTexture ltexture, int x, int y, SDL_Rect* clip);
void renderFlip(LTexture ltexture, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip);
int getWidth(LTexture ltexture);
int getHeight(LTexture ltexture);
