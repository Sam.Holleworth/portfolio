
typedef struct LTimer
{
  //The clock time when the timer started
  Uint32 mStartTicks;
  //The ticks stored when the timer started
  Uint32 mPausedTicks;
  //The timer status
  bool mPaused;
  bool mStarted;
}LTimer;
