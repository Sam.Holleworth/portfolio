
typedef struct Dot
{
  int mPosX;
  int mPosY;

  int mVelX;
  int mVelY;

  int mGrav;

  int jumpY;

  SDL_Rect mCollider;
} Dot;
