#include "main.h"
#include "LItem.h"

void makeItems(LItem items[])
{
  items[0].iPosY = 380;
  items[0].iPosX = ((SCREEN_WIDTH - items[0].iCollider.w)/4)*3;

  items[1].iPosY = 280;
  items[1].iPosX = (SCREEN_WIDTH - items[0].iCollider.w)/4;

  items[2].iPosY = 180;
  items[2].iPosX = ((SCREEN_WIDTH - items[0].iCollider.w)/4)*3;

  int i;
  for(i = 0; i < NUM_OF_ITEMS; i++)
  {
    items[i].height = items[i].iPosY;
    items[i].iCollider.x = items[i].iPosX;
    items[i].iCollider.y = items[i].iPosY;
    items[i].iCollider.h = 15;
    items[i].iCollider.w = 15;
  }
}

void animateItem(LItem items[])
{

  int i;
  for(i = 0; i < NUM_OF_ITEMS; i++)
  {
    //If item is at bottom of cycle up is true
    if(items[i].height == items[i].iPosY)
    {
      up = true;
    }
    //If item is at top of cycle up is false
    if(items[i].height == items[i].iPosY + 12)
    {
      up = false;

    }

    //Item moves up
    if(up)
    {
      items[i].iPosY -= 0.25;
      items[i].iCollider.y = items[i].iPosY;
    }
    //Item moves down
    else
    {
      items[i].iPosY += 0.25;
      items[i].iCollider.y = items[i].iPosY;
    }
    items[i].iCollider.x = items[i].iPosX;
  }

}

void collect(Dot dot, LItem items[])
{
  int i;
  for(i = 0; i < NUM_OF_ITEMS; i++)
  {
    if(checkCollision(dot.mCollider, items[i].iCollider))
    {
      items[i].iPosY = -100;
      items[i].iPosX = -100;
      score++;
    }
  }
}
