#include "main.h"
#include "LEnemy.h"

void makeEnemies(LEnemy enemies[])
{
  enemies[0].iPosY = 60;
  enemies[0].iPosX = (SCREEN_WIDTH - enemies[0].iCollider.w)/2;


  int i;
  for(i = 0; i < NUM_OF_ENEMIES; i++)
  {
    enemies[i].iCollider.x = enemies[i].iPosX;
    enemies[i].iCollider.y = enemies[i].iPosY;
    enemies[i].iCollider.h = 40;
    enemies[i].iCollider.w = 20;
  }
}

void animateEnemy(LEnemy enemies[], SDL_Rect walls[])
{

  int i;

  for(i = 0; i < NUM_OF_ENEMIES; i++)
  {
    int j;
    for(j = 0; j < NUM_OF_WALLS; j++)
    {

      //If enemy is at right of platform move left
      if((enemies[i].iPosX + enemies[i].iCollider.w == walls[j].x + walls[j].w) && (enemies[i].iPosY + enemies[i].iCollider.h == walls[j].y))
      {
        enemyLeft = true;
      }
      //If enemy is at left of platform move right
      if(enemies[i].iPosX == walls[j].x && enemies[i].iPosY + enemies[i].iCollider.h == walls[j].y)
      {
        enemyLeft = false;
      }
    }

      //Enemy moves left
      if(enemyLeft)
      {
        enemies[i].iPosX -= 1;
        enemies[i].iCollider.x = enemies[i].iPosX;
      }
      //Enemy moves right
      else
      {
        enemies[i].iPosX += 1;
        enemies[i].iCollider.x = enemies[i].iPosX;
      }
      enemies[i].iCollider.y = enemies[i].iPosY;
  }
}

void contact(Dot dot, LEnemy enemies[])
{
  int i;
  for(i = 0; i < NUM_OF_ENEMIES; i++)
  {
    if(checkCollision(dot.mCollider, enemies[i].iCollider))
    {
      running = false;
      resetted = false;
    }
  }
}
