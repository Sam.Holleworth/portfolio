#include "main.h"
#include "LPortal.h"

LPortal makePortal(LPortal portal)
{

  portal.iCollider.w = 20;
  portal.iCollider.h = 2;

  portal.iPosY = 98;
  portal.iPosX = 140;
  portal.iCollider.y = portal.iPosY;
  portal.iCollider.x = portal.iPosX;

  portal.height = portal.iPosY;

  return portal;
}

LPortal animatePortal(LPortal portal)
{
  //If item is at bottom of cycle up is true
  if(portal.height == portal.iPosY)
  {
    grow = true;
  }
  //If item is at top of cycle up is false
  if(portal.height == portal.iPosY + 5)
  {
    grow = false;
  }

  //Item moves up
  if(grow)
  {
    portal.iCollider.h += 1;
    portal.iPosY -= 1;
    portal.iCollider.y = portal.iPosY;
  }
  //Item moves down
  else
  {
    portal.iCollider.h -= 1;
    portal.iPosY += 1;
    portal.iCollider.y = portal.iPosY;
  }
  portal.iCollider.x = portal.iPosX;

  return portal;
}


void enter(Dot dot, LPortal portal)
{
  if(checkCollision(dot.mCollider, portal.iCollider) && score == NUM_OF_ITEMS)
  {
    running = false;
    won = true;
    resetted = false;
  }
}
