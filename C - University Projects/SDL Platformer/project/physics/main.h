#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "textureStructure.h"
#include "timerStructure.h"
#include "dotStructure.h"
#include "itemStructure.h"
#include "enemyStructure.h"
#include "portalStructure.h"

bool init();
bool loadMedia();
void clean();
bool checkCollision(SDL_Rect a, SDL_Rect b);
Dot canJump(Dot dot, SDL_Rect wall[]);
Dot checkCeiling(Dot dot, SDL_Rect walls[]);
void setForegroundColour();
void setBackgroundColour();

static const int SCREEN_WIDTH = 640;
static const int SCREEN_HEIGHT = 480;

//The dimensions of dot
#define DOT_WIDTH  20
#define DOT_HEIGHT 20

//Maximum axis velocity of dot
#define DOT_VEL 10
#define DOT_FALLING_VEL 15
#define DOT_JUMPING_VEL 30

//Gravity on ddot
#define DOT_GRAV 5

//Maximum jump height
#define JUMP_HEIGHT 200

#define NUM_OF_WALLS 4

#define NUM_OF_ITEMS 3

#define NUM_OF_ENEMIES 1

#define FULL_SCORE 3

//Tracks whether cube is falling
bool isFalling;

//Main menu loop flag
bool launched;

//Running loop flag
bool running;

//Win loop flag
bool won;

//Reset flag
bool resetted;

//Item animate flag
bool up;

//Portal animate flag
bool grow;

//Enemy animate flag
bool enemyLeft;

//Score keeper
int score;

//Array holds all wall structures
SDL_Rect walls[NUM_OF_WALLS];

//Array holds all item structures
LItem items[NUM_OF_ITEMS];

//Array holds all enemy structures
LEnemy enemies[NUM_OF_ENEMIES];

//Portal structure
LPortal portal;

//Global window
SDL_Window* gWindow;
//Global Renderer
SDL_Renderer* gRenderer;

//Globally used font
TTF_Font *gFont;

//Dot textures
LTexture gDotTexture;

//Start menu textures
LTexture gMenuTexture;

//Win menu text texture
LTexture gWinTexture;

//Lose menu text textures
LTexture loseText;
LTexture restartText;
LTexture pointsTexture;

//Win menu text texture
LTexture winText;

//Game text texture
LTexture scoreTexture;
