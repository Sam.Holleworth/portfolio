#include "main.h"

SDL_Window* gWindow;
SDL_Renderer* gRenderer;
TTF_Font* gFont;

static const int SCREEN_WIDTH = 640;
static const int SCREEN_HEIGHT = 480;

bool init()
{
	//Set nulls
	gWindow = NULL;
	gRenderer = NULL;
	gFont = NULL;

	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				int imgFlags = IMG_INIT_PNG;
				if(!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}

				if (TTF_Init() == -1)
				{
					printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	bool success = true;

	//Open the font
	gFont = TTF_OpenFont("../images/lazy.ttf", 28);
	if(gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	else
	{
		//Render text
		SDL_Color textColor = {0, 0, 0, 255};
		loseText = loadFromRenderedText("You Lose!", textColor);
    restartText = loadFromRenderedText("TRY AGAIN?", textColor);
	}

	return success;
}

void clean()
{

	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char const *argv[]) {

  if( !init() )
  {
    printf( "Failed to initialize!\n" );
  }
  else
  {
    if( !loadMedia())
    {
      printf( "Failed to load media!\n" );
    }
    else
    {
      //Main loop flag
      bool quit = false;

      //Event handler
      SDL_Event e;

      //Stream increasing points
      char *pointsText;

      while( !quit )
      {
        //Handle events on queue
        while( SDL_PollEvent( &e ) != 0 )
        {
          //User requests quit
          if( e.type == SDL_QUIT )
          {
            quit = true;
          }


        char points[5];
        sprintf(points, "%d", SDL_GetTicks());
				pointsText = concat("Points Collected: ", points);

        SDL_Color textColor = {0, 0, 0, 255};
        pointsTexture = loadFromRenderedText(pointsText, textColor);

        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear( gRenderer );

        render(loseText, (SCREEN_WIDTH - getWidth(loseText))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 - 50 , NULL);
        render(pointsTexture, (SCREEN_WIDTH - getWidth(pointsTexture))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 , NULL);
        render(restartText, (SCREEN_WIDTH - getWidth(restartText))/2, (SCREEN_HEIGHT - getHeight(loseText))/2 + 50 , NULL);

        //Update screen
        SDL_RenderPresent(gRenderer);
      }
    }

  //Free resources and close SDL
  clean();
  }
}
return 0;
}
