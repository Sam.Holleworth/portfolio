#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "textureStructure.h"
#include "LTexture.h"
#include "concat.h"

bool init();
bool loadMedia();
void clean();
SDL_Texture* loadTexture( char path[] );

//Global window
SDL_Window* gWindow;
//Global Renderer
SDL_Renderer* gRenderer;

//Globally used font
TTF_Font *gFont;

//Rendered texture
LTexture loseText;
LTexture restartText;

LTexture pointsTexture;
