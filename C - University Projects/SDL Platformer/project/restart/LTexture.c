#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "LTexture.h"

void constructLTexture(LTexture ltexture, SDL_Texture* t, int w, int h)
{
  ltexture.mTexture = t;
  ltexture.mWidth = w;
  ltexture.mHeight = h;
}

/////////////////////////////////////////////////////////////////////////////

void deallocate(LTexture ltexture)
{
  release(ltexture);
}

/////////////////////////////////////////////////////////////////////////////

LTexture loadFromFile(char path[])
{
  LTexture ltexture;
  release(ltexture);
  SDL_Texture* newTexture = NULL;

  SDL_Surface* loadedSurface = IMG_Load(path);
  if(loadedSurface == NULL)
  {
    printf("Unable to load image %s. SDL_image Error: %s\n", path, IMG_GetError());
  }
  else
  {
    SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
    newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
    if(newTexture == NULL)
    {
      printf("Unable to load texture image %s. SDL Error: %s\n", path, SDL_GetError());
    }
    else
    {
      ltexture.mWidth = loadedSurface->w;
      ltexture.mHeight = loadedSurface->h;
    }

    SDL_FreeSurface(loadedSurface);
  }

  ltexture.mTexture = newTexture;
  return ltexture;
}

////////////////////////////////////////////////////////////////////////////////

LTexture loadFromRenderedText( char textureText[], SDL_Color textColor)
{

  LTexture tempTexture;
  release(tempTexture);

  SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText, textColor);
  if(textSurface == NULL)
  {
    printf("Unable to load text surface. SDL Error: %s\n", SDL_GetError());
  }
  else
  {
    //Create texture from surface pixels
    tempTexture.mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
    if(tempTexture.mTexture == NULL)
    {
      printf("Unable to load text texture. SDL Error: %s\n", SDL_GetError());
    }
    else{
      //Get image dimensions
      tempTexture.mWidth = textSurface->w;
      tempTexture.mHeight = textSurface->h;
    }
    //Get rid of old surface
    SDL_FreeSurface(textSurface);
  }
  //Return LTexture
  return tempTexture;
}

///////////////////////////////////////////////////////////////////////////////////

void release(LTexture ltexture)
{
  if(ltexture.mTexture != NULL)
  {
    SDL_DestroyTexture(ltexture.mTexture);
    ltexture.mTexture = NULL;
    ltexture.mWidth = 0;
    ltexture.mHeight = 0;
  }
}

/////////////////////////////////////////////////////////////////////////////////

void setColour(LTexture ltexture, Uint8 red, Uint8 green, Uint8 blue)
{
  SDL_SetTextureColorMod(ltexture.mTexture, red, green, blue);
}

////////////////////////////////////////////////////////////////////////////////

void render(LTexture ltexture, int x, int y, SDL_Rect* clip)
{
  //Set rendering space and render to screen
  SDL_Rect renderQuad = { x, y, ltexture.mWidth, ltexture.mHeight};

  //Set clip rotation
  if(clip != NULL)
  {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  //Render to screen
  SDL_RenderCopy( gRenderer, ltexture.mTexture, clip, &renderQuad);
}

//////////////////////////////////////////////////////////////////////////////////

void renderFlip(LTexture ltexture, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
  //Set rendering space and render to screen
  SDL_Rect renderQuad = { x, y, ltexture.mWidth, ltexture.mHeight};

  //Set clip dimension
  if(clip != NULL)
  {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  //Render to screen
  SDL_RenderCopyEx( gRenderer, ltexture.mTexture, clip, &renderQuad, angle, center, flip);
}

////////////////////////////////////////////////////////////////////////////////

void setBlendMode(LTexture ltexture, SDL_BlendMode blending)
{
  SDL_SetTextureBlendMode(ltexture.mTexture, blending);
}

//////////////////////////////////////////////////////////////////////////////////

void setAlpha(LTexture ltexture, Uint8 alpha)
{
SDL_SetTextureAlphaMod(ltexture.mTexture, alpha);
}

///////////////////////////////////////////////////////////////////////////////

int getWidth(LTexture ltexture)
{
  return ltexture.mWidth;
}

//////////////////////////////////////////////////////////////////////////////

int getHeight(LTexture ltexture)
{
  return ltexture.mHeight;
}

//////////////////////////////////////////////////////////////////////////////
