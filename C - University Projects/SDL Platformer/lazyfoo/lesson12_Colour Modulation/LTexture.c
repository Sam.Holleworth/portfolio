#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "LTexture.h"

void constructLTexture(LTexture ltexture, SDL_Texture* t, int w, int h)
{
  ltexture.mTexture = t;
  ltexture.mWidth = w;
  ltexture.mHeight = h;
}

/////////////////////////////////////////////////////////////////////////////

void deallocate(LTexture ltexture)
{
  release(ltexture);
}

/////////////////////////////////////////////////////////////////////////////

LTexture loadFromFile(char path[])
{
  LTexture ltexture;
  release(ltexture);
  SDL_Texture* newTexture = NULL;

  SDL_Surface* loadedSurface = IMG_Load(path);
  if(loadedSurface == NULL)
  {
    printf("Unable to load image %s. SDL_image Error: %s\n", path, IMG_GetError());
  }
  else
  {
    SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
    newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
    if(newTexture == NULL)
    {
      printf("Unable to load texture image %s. SDL Error: %s\n", path, SDL_GetError());
    }
    else
    {
      ltexture.mWidth = loadedSurface->w;
      ltexture.mHeight = loadedSurface->h;
    }

    SDL_FreeSurface(loadedSurface);
  }

  ltexture.mTexture = newTexture;
  return ltexture;
}

///////////////////////////////////////////////////////////////////////////////////

void release(LTexture ltexture)
{
  if(ltexture.mTexture != NULL)
  {
    SDL_DestroyTexture(ltexture.mTexture);
    ltexture.mTexture = NULL;
    ltexture.mWidth = 0;
    ltexture.mHeight = 0;
  }
}

/////////////////////////////////////////////////////////////////////////////////

void setColour(LTexture ltexture, Uint8 red, Uint8 green, Uint8 blue)
{
  SDL_SetTextureColorMod(ltexture.mTexture, red, green, blue);
}

void render(LTexture ltexture, int x, int y, SDL_Rect* clip)
{
  SDL_Rect renderQuad = { x, y, ltexture.mWidth, ltexture.mHeight};

  if(clip != NULL)
  {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  SDL_RenderCopy( gRenderer, ltexture.mTexture, clip, &renderQuad);
}

//////////////////////////////////////////////////////////////////////////////////

int getWidth(LTexture ltexture)
{
  return ltexture.mWidth;
}

//////////////////////////////////////////////////////////////////////////////

int getHeight(LTexture ltexture)
{
  return ltexture.mHeight;
}

//////////////////////////////////////////////////////////////////////////////
