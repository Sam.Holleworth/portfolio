#ifndef MAIN_H
#define MAIN_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>



static const int SCREEN_WIDTH = 480;
static const int SCREEN_HEIGHT = 640;

//Starts up SDL and creates window

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( char path[] );

//The window we'll be rendering to
SDL_Window* gWindow;

//The window renderer
SDL_Renderer* gRenderer;

//Current displayed texture
SDL_Texture* gTexture;

#endif
