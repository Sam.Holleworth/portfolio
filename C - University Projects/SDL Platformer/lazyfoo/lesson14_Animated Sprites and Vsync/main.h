#include "textureStructure.h"
bool init();

bool loadMedia();

void clean();

SDL_Texture* loadTexture( char path[] );

//Global window
SDL_Window* gWindow;
//Global Renderer
SDL_Renderer* gRenderer;

//Walking animation
#define WALKING_ANIMATION_FRAMES 4
SDL_Rect gSpriteClips[WALKING_ANIMATION_FRAMES];
LTexture gSpriteSheetTexture;
