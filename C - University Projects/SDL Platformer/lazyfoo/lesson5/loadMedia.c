#include "main.h"

bool loadMedia()
{
  bool success = true;

  gStretchedSurface = loadSurface( "../images/LAND.BMP");
  if( gStretchedSurface == NULL )
  {
    printf( "Failed to load stretching image!\n");
    success = false;
  }

  return success;
}
