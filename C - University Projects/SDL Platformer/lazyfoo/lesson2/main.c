#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;


SDL_Window* window = NULL;

SDL_Surface* screensurface = NULL;

SDL_Surface* mario = NULL;


bool init()
{
  bool success = true;

  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
  {
    printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    success = false;
  }
  else
  {
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 500, SDL_WINDOW_SHOWN);
    if( window == NULL )
    {
      printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
      success = false;
    }
    else
    {
      screensurface = SDL_GetWindowSurface(window);
    }
  }
  return success;
}

bool loadMedia()
{
  bool success = true;

  mario = SDL_LoadBMP("LAND2.BMP");
  if(mario == NULL)
  {
    printf("Unable to load image %s! SDL_Error: %s\n", "LAND2.BMP", SDL_GetError());
    success = false;
  }
  return success;
}

void cleanUp()
{
  SDL_FreeSurface(mario);
  mario = NULL;

  SDL_DestroyWindow(window);

  SDL_Quit();
}

int main(int argc, char const *argv[])
{

  if(!init())
  {
    printf("Failed to initialize.\n");
  }
  else
  {
    if(!loadMedia())
    {
      printf("Failed to load media.\n");
    }
    else
    {
      SDL_BlitSurface(mario, NULL, screensurface, NULL);
      SDL_UpdateWindowSurface(window);
      SDL_Delay(5000);
    }
  }

  cleanUp();

  return 0;
}
