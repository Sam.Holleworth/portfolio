#include "main.h"

SDL_Surface* loadSurface(char path[])
{
  SDL_Surface* optimizedSurface = NULL;

  SDL_Surface* loadedSurface = IMG_Load(path);
  if(loadedSurface == NULL)
  {
    printf("Unable to load image %s! SDL Error: %s \n", path, SDL_GetError ());
  }
  else
  {
    optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, 0);
    if(optimizedSurface == NULL)
    {
      printf("Unable to optimize image %s. SDL Error: %s\n", path, SDL_GetError());
    }

    SDL_FreeSurface(loadedSurface);
  }
  return optimizedSurface;
}
