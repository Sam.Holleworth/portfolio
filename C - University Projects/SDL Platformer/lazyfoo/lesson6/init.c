#include "main.h"

bool init()
{
  bool success = true;

  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
  {
    printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    success = false;
  }
  else
  {
    gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if( gWindow == NULL )
    {
      printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
      success = false;
    }
    else
    {
        int imgFlags = IMG_INIT_PNG;
        if( !( IMG_Init( imgFlags ) & imgFlags ) )
        {
          printf("SDL image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
          success = false;
        }
        else
        {
        gScreenSurface = SDL_GetWindowSurface(gWindow);
        }
      }
    }
  return success;
}
