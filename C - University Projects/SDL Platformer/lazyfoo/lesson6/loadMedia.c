#include "main.h"

bool loadMedia()
{
  bool success = true;

  gPNGSurface = loadSurface( "../images/Small-mario.png");
  if( gPNGSurface == NULL )
  {
    printf( "Failed to load surface image!\n");
    success = false;
  }

  return success;
}
