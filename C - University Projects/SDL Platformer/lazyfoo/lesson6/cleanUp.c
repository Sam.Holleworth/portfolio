#include "main.h"

void cleanUp()
{
  SDL_FreeSurface(gPNGSurface);
  gPNGSurface = NULL;

  SDL_DestroyWindow(gWindow);
  gWindow = NULL;

  IMG_Quit();
  SDL_Quit();
}
