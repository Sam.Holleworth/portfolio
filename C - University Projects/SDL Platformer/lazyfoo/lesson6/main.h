#ifndef _MAIN_H_
  #define _MAIN_H_

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

const int SCREEN_WIDTH;
const int SCREEN_HEIGHT;

SDL_Window* gWindow;

SDL_Surface* gScreenSurface;

SDL_Surface* gStretchedSurface;

SDL_Surface* currentSurface;

SDL_Surface* gPNGSurface;

SDL_Texture* loadTexture();

bool init();
bool loadMedia();
void cleanUp();
SDL_Surface* loadSurface();
void setNulls();

#endif
