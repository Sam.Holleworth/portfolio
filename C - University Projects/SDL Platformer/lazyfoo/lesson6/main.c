#include "main.h"


const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int argc, char const *argv[])
{

  setNulls();

  if(!init())
  {
    printf("Failed to initialize.\n");
  }
  else
  {
    if(!loadMedia())
    {
      printf("Failed to load media.\n");
    }
    else
    {
      bool quit = false;

      SDL_Event e;

      while (!quit)
      {


        while(SDL_PollEvent(&e) != 0)
        {
          if(e.type == SDL_QUIT)
          {
            quit = true;
          }

        }

        SDL_Rect stretchRect;
        stretchRect.x=0;
        stretchRect.y=0;
        stretchRect.w=SCREEN_WIDTH;
        stretchRect.h=SCREEN_HEIGHT;
        SDL_BlitScaled( gStretchedSurface, NULL, gScreenSurface, &stretchRect);

        SDL_BlitSurface(gPNGSurface, NULL, gScreenSurface, NULL);

        SDL_UpdateWindowSurface(gWindow);
      }
    }
  }

  cleanUp();

  return 0;
}
