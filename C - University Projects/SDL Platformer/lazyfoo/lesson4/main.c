#include "main.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int argc, char const *argv[])
{

  setNulls();

  if(!init())
  {
    printf("Failed to initialize.\n");
  }
  else
  {
    if(!loadMedia())
    {
      printf("Failed to load media.\n");
    }
    else
    {

      currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];

      bool quit = false;

      SDL_Event e;

      while (!quit)
      {


        while(SDL_PollEvent(&e) != 0)
        {
          if(e.type == SDL_QUIT)
          {
            quit = true;
          }
          else if(e.type == SDL_KEYDOWN)
          {

            switch(e.key.keysym.sym)
            {
              case SDLK_UP:
              currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_UP];
              break;

              case SDLK_DOWN:
              currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_DOWN];
              break;

              case SDLK_LEFT:
              currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_LEFT];
              break;

              case SDLK_RIGHT:
              currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_RIGHT];
              break;

              default:
              currentSurface = keyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
              break;
            }
          }
        }

        SDL_BlitSurface(currentSurface, NULL, screensurface, NULL);

        SDL_UpdateWindowSurface(window);
      }
    }
  }

  cleanUp();

  return 0;
}
