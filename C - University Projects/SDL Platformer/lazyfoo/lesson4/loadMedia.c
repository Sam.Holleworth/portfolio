#include "main.h"

bool loadMedia()
{
  bool success = true;

  keyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadSurface("images/LAND.BMP");
  if(keyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL)
  {
    printf("Failed to load default image.\n");
    success = false;
  }

  keyPressSurfaces[KEY_PRESS_SURFACE_UP] = loadSurface("images/LAND2.BMP");
  if(keyPressSurfaces[KEY_PRESS_SURFACE_UP] == NULL)
  {
    printf("Failed to load up image.\n");
    success = false;
  }

  keyPressSurfaces[KEY_PRESS_SURFACE_DOWN] = loadSurface("images/LAND3.BMP");
  if(keyPressSurfaces[KEY_PRESS_SURFACE_DOWN] == NULL)
  {
    printf("Failed to load down image.\n");
    success = false;
  }

  keyPressSurfaces[KEY_PRESS_SURFACE_LEFT] = loadSurface("images/MARBLES.BMP");
  if(keyPressSurfaces[KEY_PRESS_SURFACE_LEFT] == NULL)
  {
    printf("Failed to load left image.\n");
    success = false;
  }

  keyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] = loadSurface("images/RAY.BMP");
  if(keyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] == NULL)
  {
    printf("Failed to load right image.\n");
    success = false;
  }

  return success;
}
