#ifndef _MAIN_H_
  #define _MAIN_H_

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <SDL2/SDL.h>

enum KeyPressSurfaces
{
  KEY_PRESS_SURFACE_DEFAULT,
  KEY_PRESS_SURFACE_UP,
  KEY_PRESS_SURFACE_DOWN,
  KEY_PRESS_SURFACE_LEFT,
  KEY_PRESS_SURFACE_RIGHT,
  KEY_PRESS_SURFACE_TOTAL
};

SDL_Window* window;

SDL_Surface* screensurface;

SDL_Surface* mario;

SDL_Surface* keyPressSurfaces[KEY_PRESS_SURFACE_TOTAL];

SDL_Surface* currentSurface;

bool init();
bool loadMedia();
void cleanUp();
SDL_Surface* loadSurface();
void setNulls();

#endif
