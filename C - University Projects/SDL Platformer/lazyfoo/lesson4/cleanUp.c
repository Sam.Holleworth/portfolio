#include "main.h"

void cleanUp()
{
  SDL_FreeSurface(mario);
  mario = NULL;

  SDL_DestroyWindow(window);

  SDL_Quit();
}
