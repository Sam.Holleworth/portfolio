
void constructLTexture(LTexture ltexture, SDL_Texture* t, int w, int h);
void deallocate(LTexture ltexture);
LTexture loadFromFile( char path[]);
void release(LTexture ltexture);
void render(LTexture ltexture, int x, int y);
int getWidth(LTexture ltexture);
int getHeight(LTexture ltexture);
