#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "LTexture.h"



static const int SCREEN_WIDTH = 640;
static const int SCREEN_HEIGHT = 480;

/////////////////////////////////////////////////////////////////////////////

bool init()
{
	gWindow = NULL;
	gRenderer = NULL;
	gTexture = NULL;
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
		}
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool loadMedia()
{
	bool success = true;

	//Load front alpha texture
	gModulatedTexture = loadFromFile("../images/fadeout.png");
	if( gModulatedTexture.mTexture == NULL )
	{
		printf("Unable to load modulated image. SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		setBlendMode(gModulatedTexture, SDL_BLENDMODE_BLEND);
	}

	//Load background texture
	gBackgroundTexture = loadFromFile("../images/fadein.png");
	if( gBackgroundTexture.mTexture == NULL)
	{
		printf("Unable to load background image. SDL Error: %s\n", SDL_GetError());
		success = false;
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

SDL_Texture* loadTexture( char path[] )
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path);
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path, IMG_GetError() );
	}
	else
	{
    //Create texture from surface pixels
    newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path, SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}
	return newTexture;
}

///////////////////////////////////////////////////////////////////////////////

void clean()
{
	//Free loaded image
	release(gFooTexture);
	release(gBackgroundTexture);

	SDL_DestroyTexture(gTexture);
	gTexture = NULL;

	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

//////////////////////////////////////////////////////////////////////////////

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{

		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			//Modulation components
			Uint8 a = 255;

			//While application is running
			while( !quit )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}
					else if(e.type == SDL_KEYDOWN)
					{
						if(e.key.keysym.sym == SDLK_w)
						{
							//Cap if over 255
							if(a + 32 > 255)
							{
								a = 255;
							}
							//Increment otherwise
							else
							{
								a += 32;
							}
						}
						else if(e.key.keysym.sym == SDLK_s)
						{
							//Cap is below 0
							if( a - 32 < 0)
							{
								a = 0;
							}
							else
							{
								a -= 32;
							}
						}
					}
				}

				//Clear screen
        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear( gRenderer );

				//Render background
				render(gBackgroundTexture, 0, 0, NULL);

				//Modulate front blended
				setAlpha(gModulatedTexture, a);
				render(gModulatedTexture, 0, 0, NULL);

        //Update screen
        SDL_RenderPresent(gRenderer);
			}
		}
	}

	//Free resources and close SDL
	clean();

	return 0;
}
