#include "textureStructure.h"
bool init();

bool loadMedia();

void clean();

SDL_Texture* loadTexture( char path[] );

//Global window
SDL_Window* gWindow;
//Global Renderer
SDL_Renderer* gRenderer;

//Arrow texture
LTexture gArrowTexture;
