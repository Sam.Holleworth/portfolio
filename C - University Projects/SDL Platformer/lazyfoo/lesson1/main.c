#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();

bool loadMedia();

bool cleanUp();

int main(int argc, char const *argv[]) {

  SDL_Window* window = NULL;

  SDL_Surface* screensurface = NULL;

  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
  {
    printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
  }
  else {
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 500, 0);
    if( window == NULL ){
      printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
    }
    else{
      screensurface = SDL_GetWindowSurface(window);

      SDL_FillRect(screensurface, NULL, SDL_MapRGB(screensurface->format, 0xFF, 0xFF, 0xFF));

      SDL_UpdateWindowSurface(window);

      SDL_Delay(2000);
    }
  }

  SDL_DestroyWindow(window);

  SDL_Quit();

  return 0;
}
