#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

static const int width = 800;
static const int height = 600;

int main(int argc, char **argv) {

  SDL_Init(SDL_INIT_VIDEO);


  SDL_Window *window = SDL_CreateWindow("Hello World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_OPENGL);


  SDL_Delay(2000);

  SDL_DestroyWindow(window);
  SDL_Quit();


  return 0;
}
