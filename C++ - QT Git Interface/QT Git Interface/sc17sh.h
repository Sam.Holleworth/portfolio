#include <QtWidgets>
#include "gitpp.h"
#include <iostream>
#include <iostream>
#include <unistd.h>

class MyWindow : public QWidget
{
Q_OBJECT

public:
  MyWindow();
  virtual ~MyWindow() {};
  void arrangeWidgets();

public slots:
  void searchCommits();

public:
  void makeConnection();

private:
  QLabel *searchLabel;
  QLineEdit *searchBar;
  QTableWidget *commitTable;
  QPushButton *searchButton;;
};
