

#include <QtWidgets>
#include <QCheckBox>
#include "sc17j2h.h"
#include <QSignalMapper>

configureTab::configureTab(){
  createWidgets();
  arrangeWidgets();
  setMinimumSize(640,400);

}

void configureTab::enableField(QWidget* fieldName){
  fieldName -> setEnabled(true);
  if(apply->isEnabled()==false){
    apply-> setEnabled(true);
  }
}

void configureTab::getConfigValues(){
  try{
    GITPP::REPO r(path);
    static auto c = r.config();

    nameValue
    -> setText(QString::fromStdString(c["user.name"].value()));
    emailValue
    -> setText(QString::fromStdString(c["user.email"].value()));

    try{
        //might not exist
        branchMasterValue
          -> setText(QString::fromStdString(c["branch.master.remote"].value()));
      }catch(...){
        branchMasterValue-> setText("Was not in .config file");
        fullConfigFile = false;
      }

      try{
        branchMasterMergeValue
        -> setText(QString::fromStdString(c["branch.master.merge"].value()));

      }catch(...){
        branchMasterMergeValue-> setText("Was not in .config file");
        fullConfigFile = false;
      }

      try{
        remoteOriginFetchValue
        -> setText(QString::fromStdString(c["remote.origin.fetch"].value()));
      } catch(...){
        remoteOriginURLValue-> setText("Was not in .config file");
        fullConfigFile = false;
      }

      try{
        remoteOriginURLValue
        -> setText(QString::fromStdString(c["remote.origin.url"].value()));

      }catch(...){
        remoteOriginFetchValue-> setText("Was not in .config file");
        fullConfigFile = false;

      }

    reposFormatVersionValue
    -> setText(QString::fromStdString(c["core.repositoryformatversion"].value()));


    if(c["core.bare"].value() == "true"){
      std::cout << "corebare true";
      checkbox ->setChecked(1);
    }
    if(c["core.logallrefupdates"].value() == "true"){
      std::cout << "logal true";
      checkbox2 ->setChecked(1);
    }
    if(c["core.filemode"].value() == "true"){
      std::cout << "filemode true";
      checkbox3 ->setChecked(1);
    }
  } catch(GITPP::EXCEPTION_CANT_FIND const& e){
    nameValue -> setText("Could not find repo!");

    msgBox->setText("Could not find a repository for Configure Tab!");
    msgBox->exec();
  }
}

void configureTab::setWidgetsDisabled(){
  nameValue -> setEnabled(false);
  emailValue -> setEnabled(false);
  branchMasterValue -> setEnabled(false);
  reposFormatVersionValue -> setEnabled(false);
  branchMasterMergeValue -> setEnabled(false);
  remoteOriginFetchValue -> setEnabled(false);
  remoteOriginURLValue -> setEnabled(false);
  apply -> setEnabled(false);

  checkbox->setEnabled(false);
  checkbox2->setEnabled(false);
  checkbox3->setEnabled(false);
}

void configureTab::applyChanges(){
  try{
    GITPP::REPO r(path.c_str());
    static auto c = r.config();
    c["user.name"] = nameValue->text().toStdString();
    c["user.email"] = emailValue->text().toStdString();
    c["core.repositoryformatversion"] = reposFormatVersionValue->text().toStdString();

    // if(fullConfigFile==false){
    //   if(branchMasterValue->text().toStdString() != "Was not in .config file"){
    //     c["branch.master.remote"] = branchMasterValue->text().toStdString();
    //   }
    //   if(branchMasterMergeValue->text().toStdString() != "Was not in .config file"){
    //     c["branch.master.merge"] = branchMasterMergeValue->text().toStdString();
    //   }
    //   if(remoteOriginFetchValue->text().toStdString() != "Was not in .config file"){
    //     c["remote.origin.fetch"] = remoteOriginFetchValue->text().toStdString();
    //   }
    //   if(remoteOriginURLValue->text().toStdString() != "Was not in .config file"){
    //     c["remote.origin.url"] = remoteOriginURLValue->text().toStdString();
    //   }
    // }

    // void checkFieldContents(string value,string cstring){
    //   if (value != "Was not in .config file"){
    //     c[cstring] = value;
    //   }
    // }

    if(checkbox ->isChecked()){
      c["core.bare"] = "true";
    }else{
      c["core.bare"] = "false";
    }

    if(checkbox2 ->isChecked()){
      c["core.logallrefupdates"] ="true";
    }else{
      c["core.logallrefupdates"] = "false";
    }

    if(checkbox3 ->isChecked()){
      c["core.filemode"].value() = "true";
    }else{
      c["core.filemode"].value() = "false";
    }

    setWidgetsDisabled();

  } catch(GITPP::EXCEPTION_CANT_FIND const& e){
    nameValue -> setText("Could not find repo!");
  }
}



void configureTab::cancelChanges(){
  getConfigValues();
  setWidgetsDisabled();
}

void configureTab::createWidgets(){
  getConfigValues();

  nameValue -> setMaximumWidth(290);
  emailValue -> setMaximumWidth(290);
  branchMasterValue -> setMaximumWidth(290);
  reposFormatVersionValue -> setMaximumWidth(290);
  branchMasterMergeValue -> setMaximumWidth(290);
  remoteOriginFetchValue -> setMaximumWidth(290);
  remoteOriginURLValue -> setMaximumWidth(290);

  editButton->setFixedSize(30,25);
  editButton2->setFixedSize(30,25);
  editButton3->setFixedSize(30,25);
  editButton4->setFixedSize(30,25);
  editButton5->setFixedSize(30,25);
  editButton6->setFixedSize(30,25);
  editButton7->setFixedSize(30,25);

  editButtonCb->setFixedSize(30,25);
  editButtonCb2->setFixedSize(30,25);
  editButtonCb3->setFixedSize(30,25);

  setWidgetsDisabled();

  if(!fullConfigFile){
    editButton3 -> setEnabled(false);
    editButton5 -> setEnabled(false);
    editButton6 -> setEnabled(false);
    editButton7 -> setEnabled(false);
  } //may not need to disable these.

  QSignalMapper* signalMapper = new QSignalMapper(this);
  connect(signalMapper, SIGNAL(mapped(QWidget *)), this, SLOT(enableField(QWidget *)));

  signalMapper->setMapping(editButton, nameValue);
  signalMapper->setMapping(editButton2, emailValue);
  signalMapper->setMapping(editButton3, branchMasterValue);
  signalMapper->setMapping(editButton4, reposFormatVersionValue);
  signalMapper->setMapping(editButton5, branchMasterMergeValue);
  signalMapper->setMapping(editButton6, remoteOriginFetchValue);
  signalMapper->setMapping(editButton7, remoteOriginURLValue);

  signalMapper->setMapping(editButtonCb, checkbox);
  signalMapper->setMapping(editButtonCb2, checkbox2);
  signalMapper->setMapping(editButtonCb3, checkbox3);

  connect(editButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton2, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton3, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton4, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton5, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton6, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButton7, SIGNAL(clicked()), signalMapper, SLOT(map()));

  connect(editButtonCb, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButtonCb2, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(editButtonCb3, SIGNAL(clicked()), signalMapper, SLOT(map()));

  QObject::connect(cancel, SIGNAL(clicked()), this, SLOT(cancelChanges()));
  QObject::connect(apply, SIGNAL(clicked()), this, SLOT(applyChanges()));


}

void configureTab::arrangeWidgets(){
  nameLabel -> setBuddy(nameValue);

  QHBoxLayout* nameLineLayout = new QHBoxLayout();
  nameLineLayout->addWidget(nameLabel);
  nameLineLayout->addWidget(nameValue);
  nameLineLayout->addWidget(editButton);

  QHBoxLayout* emailLayout = new QHBoxLayout();
  emailLayout->addWidget(emailLabel);
  emailLayout->addWidget(emailValue);
  emailLayout->addWidget(editButton2);

  QHBoxLayout* coreBareLayout = new QHBoxLayout();
  coreBareLayout->addWidget(coreBareLabel);
  coreBareLayout->addWidget(checkbox);
  coreBareLayout->addWidget(editButtonCb);

  QHBoxLayout* logallRefUpdatesLayout = new QHBoxLayout();
  logallRefUpdatesLayout->addWidget(logallRefUpdatesLabel);
  logallRefUpdatesLayout->addWidget(checkbox2);
  logallRefUpdatesLayout->addWidget(editButtonCb2);

  QHBoxLayout* branchMasterLayout = new QHBoxLayout();
  branchMasterLayout->addWidget(branchMasterLabel);
  branchMasterLayout->addWidget(branchMasterValue);
  branchMasterLayout->addWidget(editButton3);

  QHBoxLayout* reposFormatVersionLayout = new QHBoxLayout();
  reposFormatVersionLayout->addWidget(reposFormatVersionLabel);
  reposFormatVersionLayout->addWidget(reposFormatVersionValue);
  reposFormatVersionLayout->addWidget(editButton4);

  QHBoxLayout* branchMasterMergeLayout = new QHBoxLayout();
  branchMasterMergeLayout->addWidget(branchMasterMergeLabel);
  branchMasterMergeLayout->addWidget(branchMasterMergeValue);
  branchMasterMergeLayout->addWidget(editButton5);

  QHBoxLayout* remoteOriginFetchLayout = new QHBoxLayout();
  remoteOriginFetchLayout->addWidget(remoteOriginFetchLabel);
  remoteOriginFetchLayout->addWidget(remoteOriginFetchValue);
  remoteOriginFetchLayout->addWidget(editButton6);

  QHBoxLayout* coreFileLayout = new QHBoxLayout();
  coreFileLayout->addWidget(coreFileLabel);
  coreFileLayout->addWidget(checkbox3);
  coreFileLayout->addWidget(editButtonCb3);

  QHBoxLayout* remoteOriginURLLayout = new QHBoxLayout();
  remoteOriginURLLayout->addWidget(remoteOriginURLLabel);
  remoteOriginURLLayout->addWidget(remoteOriginURLValue);
  remoteOriginURLLayout->addWidget(editButton7);


  QHBoxLayout* buttonsLayout = new QHBoxLayout();
  QSpacerItem* item = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Fixed);
  buttonsLayout->addSpacerItem(item);
  buttonsLayout->addWidget(apply);
  buttonsLayout->addWidget(cancel);

  QVBoxLayout* configValLayout = new QVBoxLayout(); //where all config layouts go
  configValLayout -> addLayout(nameLineLayout);

  configValLayout -> addLayout(emailLayout);
  configValLayout -> addLayout(coreBareLayout);
  configValLayout -> addLayout(logallRefUpdatesLayout);
  configValLayout -> addLayout(branchMasterLayout);
  configValLayout -> addLayout(reposFormatVersionLayout);
  configValLayout -> addLayout(branchMasterMergeLayout);
  configValLayout -> addLayout(remoteOriginFetchLayout);
  configValLayout -> addLayout(coreFileLayout);
  configValLayout -> addLayout(remoteOriginURLLayout);

  QVBoxLayout* mainLayout = new QVBoxLayout();
  mainLayout->addWidget(pathLine);
  mainLayout->addWidget(firstLine);
  mainLayout->addLayout(configValLayout);
  mainLayout ->addLayout(buttonsLayout);
  this->setLayout(mainLayout);
}
