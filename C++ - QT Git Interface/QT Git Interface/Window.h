#include <QWidget>

#include "sc17sh.h"
#include "sc17nlg.h"
#include "sc17pl.h"
#include "sc17j2h.h"
#include "sc17s2s.h"

class Window : public QWidget
{
public:
  Window();


private:
  QTabWidget *tabWidget;
  MyWindow *searchTab;
  HelloWorldLabel *commitsTab;
  SelectTab *selectTab;
  configureTab * configTab;
  BranchesLabel * branchesTab;
  
};
