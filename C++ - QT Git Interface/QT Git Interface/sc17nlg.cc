#include "sc17nlg.h"

HelloWorldLabel::HelloWorldLabel() : QWidget(){

	QWidget* window = new QWidget();
	window->setWindowTitle("list of commits - sc17nlg");
	setMinimumSize(640, 400);



	QTableWidget* commitsTable = new QTableWidget();
	commitsTable->setColumnCount(5);

	commitsTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Commit message"));
	commitsTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Author"));
	commitsTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Time"));
	commitsTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Commit ID"));
	commitsTable->setHorizontalHeaderItem(4, new QTableWidgetItem("Branch"));
	commitsTable->verticalHeader()->hide();
	//commitsTable->setShowGrid(false);

	QHeaderView* header = commitsTable->horizontalHeader();
	header->setSectionResizeMode(QHeaderView::Stretch);

	GITPP::REPO r("..");
	int rowNumber = 0;
	int currentRow = 0;
	int noCommitsMade = 0;



	for(auto i : r.commits())
  {
		rowNumber++;
		commitsTable->setRowCount(rowNumber);


		commitsTable->setItem(currentRow, 0, new QTableWidgetItem(QString::fromStdString(i.message())));
		commitsTable->setItem(currentRow, 1, new QTableWidgetItem(QString::fromStdString(i.signature().name())));
		commitsTable->setItem(currentRow, 2, new QTableWidgetItem(QString::fromStdString(i.time(10))));
		commitsTable->setItem(currentRow, 3, new QTableWidgetItem(QString::fromStdString(i.id())));

		for(GITPP::BRANCH b : r.branches())

	{
		commitsTable->setItem(currentRow, 4, new QTableWidgetItem(QString::fromStdString(b.name())));
	}

		currentRow++;

		noCommitsMade++;

  }

	QLabel* commitsTitle = new QLabel("List of all commits");

	QLabel* date = new QLabel(__DATE__);
	QLabel* noCommits = new QLabel("number of commits: " + QString::number(noCommitsMade));



	QHBoxLayout* titleLayout = new QHBoxLayout();
  titleLayout->addWidget(commitsTitle);

	QHBoxLayout* commitsLayout = new QHBoxLayout();
	commitsLayout->addWidget(date);
  commitsLayout->addWidget(noCommits);

	QVBoxLayout* centreLayout = new QVBoxLayout();
	centreLayout->addLayout(titleLayout);
	centreLayout->addLayout(commitsLayout);




	commitsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

  QVBoxLayout* mainLayout = new QVBoxLayout();

	centreLayout->addLayout(titleLayout);
	centreLayout->addLayout(commitsLayout);
	centreLayout->addWidget(commitsTable);

	mainLayout->addLayout(centreLayout);
	setLayout(mainLayout);

}
