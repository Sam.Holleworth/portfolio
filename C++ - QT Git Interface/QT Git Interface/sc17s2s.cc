#include "sc17s2s.h"
#include "sc17pl.h"


static void functionS(){
	try{
		GITPP::REPO r("..");
		r.checkout("master");
		QMessageBox* check = new QMessageBox;
		check->setText("Switched to master branch");
		check->exec();
	} catch(GITPP::EXCEPTION){
		QMessageBox* error = new QMessageBox;
		error->setText("Error switching branch");
		error->exec();
	}

	// r.checkout(combo->currentText().toUtf8().constData());
}


BranchesLabel::BranchesLabel() : QWidget(){

	QVBoxLayout* mainLayout = new QVBoxLayout;

	QLabel* title = new QLabel("Branches");
	mainLayout->addWidget(title);

	QHBoxLayout* selectLayout = new QHBoxLayout;
	selectLayout->addWidget(new QLabel("Please select a branch:"));

	QComboBox* combo = new QComboBox();
	combo->addItem("Select branch");

	selectLayout->addWidget(combo);
	QPushButton* button = new QPushButton("Apply");
	selectLayout->addWidget(button);

	try
	{
		GITPP::REPO r("..");
		for(GITPP::BRANCH i : r.branches()){
			combo->addItem(QString::fromStdString(i.name()));
		}
		QObject::connect(button,&QPushButton::clicked,functionS);
	}
	catch(GITPP::EXCEPTION_CANT_FIND)
	{
		combo->addItem("No Repo");
	}


	mainLayout->addLayout(selectLayout);
	mainLayout->addWidget(new QLabel(""));

	setLayout(mainLayout);
}
