#include "sc17pl.h"

SelectTab::SelectTab() : QWidget()
{
  this->arrangeWidgets();
  this->makeConnection();
  this->setWindowTitle("My Window");
  this->setMinimumSize(640, 400);
}

void SelectTab::arrangeWidgets()
{
  QHBoxLayout *barLayout = new QHBoxLayout();
  barLayout->addWidget(selectLabel);
  QDirIterator it(".",QDir::Dirs|QDir::NoDotAndDotDot,QDirIterator::Subdirectories);
  try
    {
      GITPP::REPO r;
    }
  catch(GITPP::EXCEPTION_CANT_FIND const&)
    {
      selectDir->addItem(".");
    }
  barLayout->addWidget(selectDir);
  barLayout->addWidget(selectButton);
  QVBoxLayout *mainLayout = new QVBoxLayout();
  mainLayout->addLayout(barLayout);
  auto lineA = new QFrame;
  lineA->setFrameShape(QFrame::HLine);
  lineA->setFrameShadow(QFrame::Sunken);
  mainLayout->addWidget(lineA);
  mainLayout->addWidget(repoLabel);
  {
  QRadioButton *butt = new QRadioButton(".");
  if(selected == ".") butt->setDown(1);
  connect(butt,SIGNAL(clicked()), this, SLOT(selectRepo()));
  mainLayout->addWidget(butt);
  }
  while(it.hasNext())
    {
      QString s = it.next();
      try
	{
	  GITPP::REPO r(s.toStdString());
	  QRadioButton *butt = new QRadioButton(s);
	  if(selected == s) butt->setDown(1);
	  connect(butt,SIGNAL(clicked()), this, SLOT(selectRepo()));
	  mainLayout->addWidget(butt);
	}
      catch(GITPP::EXCEPTION_CANT_FIND const&)
	{
	  selectDir->addItem(s);
	}
    }
  mainLayout->addStretch();
  this->setLayout(mainLayout);
}

void SelectTab::selectRepo()
{
  QPushButton* object = (QPushButton*) QObject::sender();
  selected = object->text();
}

void SelectTab::createRepo()
{
  if(selectDir->currentText() == ""){
    return;
  }
  GITPP::REPO(GITPP::REPO::_create,(selectDir->currentText()).toStdString());
  selectDir->removeItem(0);
}


void SelectTab::makeConnection()
{
  connect(selectButton, SIGNAL(clicked()), this, SLOT(createRepo()));
}
