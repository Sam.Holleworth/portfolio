#include "Display.h"

int main(char* argc, int argv)
{
	Display display;
	Environment environment;
	char running = 'y';

	while (running != 'n')
	{
		display.show();
		std::cout << "Run another year? y/n \n";
		std::cin >> running;
	}
	return 0;
}