#include "Amoeba.h"

Amoeba::Amoeba(std::string type) : Cell(type) {}

Amoeba::Amoeba(std::string type, int x, int y, int parId) : Cell(type, x, y, parId) {}

Amoeba::~Amoeba(){}

void Amoeba::update()
{
	eat();
	if (Cell::checkForDivide())
	{
		divide();
	}
	//Cell::displayInfo();
	Cell::update();
}

//Create a new cell in the first available space starting from above and moving clockwise
void Amoeba::divide()
{
	checkSurroundings();
	int id = getId();
	int x = getPositionX();
	int y = getPositionY();
	int energy = getEnergy();

	if (getAdjacencyList(1) == 0 && x + 1 <= SIZE_OF_ENVIRONMENT_X)//spawn right if not blocked
	{
		ocean[x+1][y] = new Amoeba("amoeba", x + 1, y, id);
		setEnergy(energy - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(3) == 0 && x - 1 >= 0)//spawn left if not blocked
	{
		ocean[x-1][y] = new Amoeba("amoeba", x - 1, y, id);
		setEnergy(energy - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(0) == 0 && y - 1 >= 0)//spawn up if not blocked
	{
		ocean[x][y - 1] = new Amoeba("amoeba", x, y - 1, id);
		setEnergy(energy - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(2) == 0 && y + 1 <= SIZE_OF_ENVIRONMENT_Y)//spawn down if not blocked
	{
		ocean[x][y] = new Amoeba("amoeba", x, y + 1, id);
		setEnergy(energy - ENERGY_FOR_REPRODUCTION);
	}
}

void Amoeba::eat()
{
	checkSurroundings();
	int x = getPositionX();
	int y = getPositionY();
	int energy = getEnergy();

	if (getAdjacencyList(0) != NULL && y - 1 >= 0)
	{
		Cell* cell = getAdjacencyList(0);
		if (cell->getType() == "algae")
		{
			setEnergy(energy + cell->getEnergy());
			cell->die();
			cell = NULL;
			setAdjacencyList(0, NULL);
		}
	}
	if (getAdjacencyList(2) != NULL && y + 1 < SIZE_OF_ENVIRONMENT_Y)
	{
		Cell* cell = getAdjacencyList(2);
		if (cell->getType() == "algae")
		{
			setEnergy(energy + cell->getEnergy());
			cell->die();
			cell = NULL;
			setAdjacencyList(2, NULL);
		}
	}
	if (getAdjacencyList(3) != NULL && x - 1 >= 0)
	{
		Cell* cell = getAdjacencyList(3);
		if (cell->getType() == "algae")
		{
			setEnergy(energy + cell->getEnergy());
			cell->die();
			cell = NULL;
			setAdjacencyList(3, NULL);
		}
	}
	if (getAdjacencyList(1) != NULL && x + 1 < SIZE_OF_ENVIRONMENT_X)
	{
		Cell* cell = getAdjacencyList(1);
		if (cell->getType() == "algae")
		{
			setEnergy(energy + cell->getEnergy());
			cell->die();
			cell = NULL;
			setAdjacencyList(1, NULL);
		}
	}

}

void Amoeba::displayEnergy()
{
	std::cout << getId() << " : " << getEnergy() << "\n"; 
}
