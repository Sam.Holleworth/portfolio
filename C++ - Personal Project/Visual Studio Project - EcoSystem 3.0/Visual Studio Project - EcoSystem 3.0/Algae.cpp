#include "Algae.h"

Algae::Algae(std::string type) : Cell(type){}

Algae::Algae(std::string type, int x, int y, int parId) : Cell(type, x, y, parId){}

Algae::~Algae(){}

//Update @Overidden to contain photosynthesize
void Algae::update()
{
	photosynthesize();
	if (Cell::checkForDivide())
	{
		divide();
	}
	//displayInfo();
	Cell::update();
}

//Create a new cell in the first available space starting from above and moving clockwise
void Algae::divide()
{
	checkSurroundings();

	if (getAdjacencyList(1) == 0 && getPositionX() + 1 < SIZE_OF_ENVIRONMENT_X)//spawn right if not blocked
	{
		ocean[getPositionX() + 1][getPositionY()] = new Algae("algae", getPositionX() + 1, getPositionY(), getId());
		setEnergy(getEnergy() - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(3) == 0 && getPositionX() - 1 >= 0)//spawn left if not blocked
	{
		ocean[getPositionX() - 1][getPositionY()] = new Algae("algae", getPositionX() - 1, getPositionY(), getId());
		setEnergy(getEnergy() - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(0) == 0 && getPositionY() - 1 >= 0)//spawn up if not blocked
	{
		ocean[getPositionX()][getPositionY() - 1] = new Algae("algae", getPositionX(), getPositionY() - 1, getId());
		setEnergy(getEnergy() - ENERGY_FOR_REPRODUCTION);
	}
	else if (getAdjacencyList(2) == 0 && getPositionY() + 1 < SIZE_OF_ENVIRONMENT_Y)//spawn down if not blocked
	{
		ocean[getPositionX()][getPositionY() + 1] = new Algae("algae", getPositionX(), getPositionY() + 1, getId());
		setEnergy(getEnergy() - ENERGY_FOR_REPRODUCTION);
	}

}

//Absorb light from current tile
void Algae::photosynthesize()
{
	Cell::setEnergy(lightGrid[getPositionX()][getPositionY()]);
}