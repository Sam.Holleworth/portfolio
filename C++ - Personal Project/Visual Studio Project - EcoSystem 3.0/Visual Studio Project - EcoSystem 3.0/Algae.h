#ifndef _ALGAE_H_
#define _ALGAE_H_

#include "Cell.h"

class Algae :
	public Cell
{
public:
	Algae(std::string type);
	Algae(std::string type, int x, int y, int parId);
	~Algae();

	void update();
	void divide();

private:
	void photosynthesize();
};


#endif _ALGAE_H_