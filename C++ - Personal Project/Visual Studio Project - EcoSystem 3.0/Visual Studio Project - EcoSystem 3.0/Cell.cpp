#include <stdlib.h>  
#include "Cell.h"

int Cell::nextId = 0;

//Constructor for spawning a wave of cells with random locations
Cell::Cell(std::string type)
{
	id = nextId++;
	cellType = type;
	parentId = -1;

	bool spawned = false;
	while (spawned == false)
	{
		int x = rand() % SIZE_OF_ENVIRONMENT_X;
		int y = rand() % SIZE_OF_ENVIRONMENT_Y;
		if (ocean[x][y] == NULL)
		{
			position[0] = x;
			position[1] = y;
			spawned = true;
		}
	}

	age = 0;
	energy = 10;

	for (int i = 0; i < 5; i++)
	{
		adjacencyList[i] = 0;
	}

	hasMoved = false;
}

//Constructor for cell division
Cell::Cell(std::string type, int x, int y, int parId)
{
	id = nextId++;
	cellType = type;
	parentId = parId;
	position[0] = x;
	position[1] = y;
	age = 0;
	energy = 10;
	for (int i = 0; i < 5; i++)
	{
		adjacencyList[i] = 0;
	}
	hasMoved = false;
}

Cell::~Cell()
{
}

void Cell::update()
{
	move();
	birthday();
	checkIfDead();
}

/*
Function uses a random number to determine the direction moved each update
if the number is over 6 move over the x axis, if under 6 move over the y
if the number is odd add 1 to x or y, if number is even minus 1
*/
void Cell::move()
{
	int randomNumber = rand() % 12 +  1;

	checkSurroundings();

	//move on x
	if (randomNumber > 6)
	{
		if (randomNumber % 2 == 1 && adjacencyList[1] == 0 && position[0] + 1 < SIZE_OF_ENVIRONMENT_X)//move right if not blocked
		{
			ocean[position[0] + 1][position[1]] = this;
			ocean[position[0]][position[1]] = 0;
			position[0]++;
		}
		else if(adjacencyList[3] == 0 && position[0] - 1 >= 0)//move left if not blocked
		{
			ocean[position[0] - 1][position[1]] = this;
			ocean[position[0]][position[1]] = 0;
			position[0]--;
		}
	}

	//move on y
	else
	{
		if (randomNumber % 2 == 1 && adjacencyList[0] == 0 && position[1] - 1 >= 0)//move up if not blocked
		{
			ocean[position[0]][position[1] - 1] = this;
			ocean[position[0]][position[1]] = 0;
			position[1]--;
		}
		else if (adjacencyList[2] == 0 && position[1] + 1 < SIZE_OF_ENVIRONMENT_Y)//move down if not blocked
		{
			ocean[position[0]][position[1] + 1] = this;
			ocean[position[0]][position[1]] = 0;
			position[1]++;
		}
	}

	hasMoved = true;

}

void Cell::checkSurroundings()
{
	adjacencyList[4] = ocean[position[0]][position[1]];
	//right
	adjacencyList[1] = ocean[position[0] + 1][position[1]];
	//left
	adjacencyList[3] = ocean[position[0] - 1][position[1]];
	//up
	adjacencyList[0] = ocean[position[0]][position[1] - 1];
	//down
	adjacencyList[2] = ocean[position[0]][position[1] + 1];
}

//Age a cell then check if it expires
void Cell::birthday()
{
	age++;
	energy -= 5;
}

//If the cell is too old or starving kill it
void Cell::checkIfDead()
{
	if (cellType == "algae" && age >= MAX_ALGAE_AGE)
	{
		die();
	}
	else if (cellType == "amoeba" && age >= MAX_AMOEBA_AGE)
	{
		die();
	}
	else if (energy <= 0)
	{
		die();
	}
}

//Kill cell
void Cell::die()
{
	int x = position[0];
	int y = position[1];
	delete ocean[x][y];
	ocean[x][y] = NULL;
}

//If energy is high enough for division return true
bool Cell::checkForDivide()
{
	if (cellType == "algae" && energy >= ALGAE_REPRODUCTION_ENERGY)
	{
		return true;
	}
	else if (cellType == "amoeba" && energy >= AMOEBA_REPRODUCTION_ENERGY)
	{
		return true;
	}
	return false;
}

//Print info to console
void Cell::displayInfo()
{
	std::cout << "ID: " << id << "\n";
	std::cout << "Type: " << cellType << "\n";
	std::cout << "Parent ID: " << parentId << "\n";
	std::cout << "Position: [" << position[0] << "," << position[1] <<  "]\n";
	std::cout << "Age: " << age << "\n";
	std::cout << "Energy: " << energy << "\n";
	std::cout << "Adjacency List: [" << adjacencyList[0] << ", " << adjacencyList[1] << ", " << adjacencyList[2] << ", " << adjacencyList[3] << ", " << adjacencyList[4] << "]";
	std::cout << "\n\n";
}
