#include "Environment.h"

Cell* ocean[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];
int lightGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];

Environment::Environment()
{
}

Environment::~Environment()
{
}

void Environment::initialiseGridValues()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			lightGrid[i][j] = SUNLIGHT - j;
			if (lightGrid[i][j] < 0)
			{
				lightGrid[i][j] = 0;
			}
			ocean[i][j] = NULL;
		}
	}
}

void Environment::spawnAlgae(int numberOfRequestedAlgae)
{
	for (int i = 0; i < numberOfRequestedAlgae; i++)
	{
		Algae* algae = new Algae("algae");
		ocean[algae->getPositionX()][algae->getPositionY()] = algae;
	}
}

void Environment::spawnAmoeba(int numberOfRequestedAmoeba)
{
	for (int i = 0; i < numberOfRequestedAmoeba; i++)
	{
		Amoeba* amoeba = new Amoeba("amoeba");
		ocean[amoeba->getPositionX()][amoeba->getPositionY()] = amoeba;
	}
}

void Environment::run()
{
	resetMoveFlags();

	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			if (ocean[i][j] != 0 && ocean[i][j]->getHasMoved() == false)
			{
				ocean[i][j]->update();
			}
		}
	}

}

void Environment::resetMoveFlags()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			if (ocean[i][j] != NULL)
			{
				ocean[i][j]->setHasMoved(false);
			}
		}
	}
}



