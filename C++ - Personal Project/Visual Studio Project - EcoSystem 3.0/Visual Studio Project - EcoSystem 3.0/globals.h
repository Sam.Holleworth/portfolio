#pragma once

#include <iostream>
#include <string>

#include <stdlib.h>
#include <crtdbg.h>

#include"definitions.h"

class Cell;

extern Cell* ocean[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];
extern int lightGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];