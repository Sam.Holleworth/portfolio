#pragma once
#ifndef _CELL_H_
#define _CELL_H_

#include "globals.h"

class Cell
{
protected:
	static int nextId;

private:
	int id;
	std::string cellType;
	int parentId;
	int position[2];
	Cell* adjacencyList[5];
	int age;
	int energy;
	bool hasMoved;

public:
	Cell(std::string type);
	Cell(std::string type, int x, int y, int parId);
	~Cell();

	int getId() { return id; }
	std::string getType() { return cellType; }
	int getEnergy() { return energy; }
	int getPositionX() { return position[0]; }
	int getPositionY() { return position[1]; }
	Cell* getAdjacencyList(int index) { return adjacencyList[index]; }
	bool getHasMoved() { return hasMoved; }

	void setEnergy(int newEnergy) { energy = newEnergy; }
	void setAdjacencyList(int index, Cell* cell) { adjacencyList[index] = cell; }
	void setHasMoved(bool moved) { hasMoved = moved; }

	virtual void update();
	void displayInfo();
	bool checkForDivide();
	//void clamp();
	void move();
	void checkSurroundings();
	void checkIfDead();
	void die();

private:
	void birthday();


};

#endif _CELL_H_
