#pragma once
#include "Algae.h"
#include "Amoeba.h"

class Environment
{
public:
	Environment();
	~Environment();
	void initialiseGridValues();
	void spawnAlgae(int numberOfRequestedAlgae);
	void spawnAmoeba(int numberOFRequestedAmoeba);
	void run();

private:
	void resetMoveFlags();
};

