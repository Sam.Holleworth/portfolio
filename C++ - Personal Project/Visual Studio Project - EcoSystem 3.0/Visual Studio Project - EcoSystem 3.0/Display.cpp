#include "Display.h"

Display::Display()
{
	environment.initialiseGridValues();
	environment.spawnAlgae(NUMBER_OF_ALGAE);
	environment.spawnAmoeba(NUMBER_OF_AMOEBA);
}

Display::~Display()
{
}

void Display::update()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			if (ocean[i][j] != 0)
			{
				if (ocean[i][j]->getType() == "algae")
				{
					displayGrid[i][j] = "*";
				}
				else if (ocean[i][j]->getType() == "amoeba")
				{
					displayGrid[i][j] = "&";
				}
			}
			else 
			{
				displayGrid[i][j] = ".";
			}
		}
	}
	//printAddresses();
	environment.run();
	_CrtDumpMemoryLeaks();
}

//void Display::refresh()
//{
//	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
//	{
//		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
//		{
//			displayGrid[i][j] = ".";
//		}
//	}
//}

void Display::show()
{
	update();
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_Y; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_X; j++)
		{
			std::cout << displayGrid[j][i];
			if (j == SIZE_OF_ENVIRONMENT_X - 1)
			{
				std::cout << "\n";
			}
		}
	}
}

void Display::printAddresses()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_Y; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_X; j++)
		{
			std::cout << ocean[j][i] << "\n";
			if (j == SIZE_OF_ENVIRONMENT_X - 1)
			{
				std::cout << "\n";
			}
		}
	}
}
