#pragma once
#ifndef _AMOEBA_H_
#define _AMOEBA_H_

#include "Cell.h"

class Amoeba :
	public Cell
{
public:
	Amoeba(std::string type);
	Amoeba(std::string type, int x, int y, int parId);
	~Amoeba();

	void update();
	void divide();
	void eat();
	void displayEnergy();
};

#endif _AMOEBA_H_

