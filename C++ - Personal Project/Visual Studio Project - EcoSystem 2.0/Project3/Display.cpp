#include "Display.h"
#include <string>

Display::Display()
{
	environment.intializeGridValues();
	environment.spawnAlgae(10);
	environment.spawnAmoeba(10);
	refresh();
}

Display::~Display()
{
}

void Display::update(bool mode)
{
	if (mode == 0)
	{
		refresh();

		environment.run();

		std::list<Algae> algaeList = environment.getAlgaeList();
		std::list<Amoeba> amoebaList = environment.getAmeobaList();

		for (std::list<Algae>::iterator it = algaeList.begin(); it != algaeList.end(); ++it)
		{
			int x = it->getPositionX();
			int y = it->getPositionY();
			displayGrid[x][y] = "*";
			//displayGrid[x][y] = std::to_string(it->getId());
		}
		for (std::list<Amoeba>::iterator it2 = amoebaList.begin(); it2 != amoebaList.end(); ++it2)
		{
			int x = it2->getPositionX();
			int y = it2->getPositionY();
			displayGrid[x][y] = "&";
			//displayGrid[x][y] = std::to_string(it->getId());
		}

		//environment.displayAlgaeInfo();
		show();
		//showLocation();

	}
	else
	{
		environment.run();
		environment.displayCellInfo('c');
	}
}


void Display::refresh()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			displayGrid[i][j] = ".";
		}
	}
}

void Display::show()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_Y; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_X; j++)
		{
			std::cout << displayGrid[j][i];
			if (j == SIZE_OF_ENVIRONMENT_X - 1)
			{
				std::cout << "\n";
			}
		}
	}
}

void Display::showLocation()
{
	std::cout << "\n\n";

	for (int i = 0; i < SIZE_OF_ENVIRONMENT_Y; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_X; j++)
		{
			std::cout << locationGrid[j][i];
			if (j == SIZE_OF_ENVIRONMENT_X - 1)
			{
				std::cout << "\n";
			}
		}
	}
}


