#include "Environment.h"

Environment::Environment()
{
}

Environment::~Environment()
{
}

Cell* locationGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];

//fill light grid with values descending from top to bottom
void Environment::intializeGridValues()
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			lightGrid[i][j] = 25 - j;
			if (lightGrid[i][j] < 0)
			{
				lightGrid[i][j] = 0;
			}
			locationGrid[i][j] = NULL;
		}
	}
}

//Spawns a requested number of algae in random locations and adds them to the list
void Environment::spawnAlgae(int numberOfRequestedAlgae)
{
	for (int i = 0; i < numberOfRequestedAlgae; i++)
	{
		Algae algae("algae");
		locationGrid[algae.getPositionX()][algae.getPositionY()] = &algae;
		algaeList.push_back(algae);
	}
}

void Environment::spawnAmoeba(int numberOfRequestedAmoeba)
{
	for (int i = 0; i < numberOfRequestedAmoeba; i++)
	{
		Amoeba amoeba("amoeba");
		locationGrid[amoeba.getPositionX()][amoeba.getPositionY()] = &amoeba;
		amoebaList.push_back(amoeba);
	}
}

//Display information for all algae in list
void Environment::displayCellInfo(char mode)
{
	if (mode == 'a')
	{
		for (Algae algae : algaeList)
		{
			algae.displayInfo();
		}
	}
	else if (mode == 'b')
	{
		for (Amoeba amoeba : amoebaList)
		{
			amoeba.displayInfo();
		}
	}
	else if (mode == 'c')
	{
		for (Amoeba amoeba : amoebaList)
		{
			amoeba.displayInfo();
		}
		for (Algae algae : algaeList)
		{
			algae.displayInfo();
		}
	}
	else
	{
		std::cout << "Display parameter invalid, please use either a, b or c";
	}
}

//Handles adding newly born cells to list
void Environment::run()
{
	std::list<Algae>::iterator it = algaeList.begin();
	while (it != algaeList.end())
	{
		checkSurroundings(*it);

		//if the cell is on top of another move it
		while (it->getAdjacencyList(4) < 0)
		{
			it->move();
			checkSurroundings(*it);
		}

		it->update();

		checkSurroundings(*it);

		//if the cell is dead delete it
		if (!it->isAlive())
		{
			it = algaeList.erase(it);
		}
		//if the cell has enough energy and space to divide, divide it
		else if (it->checkForDivide())
		{
			algaeList.push_back(it->divide());
			++it;
		}
		//else just carry onto the next cell
		else
		{
			++it;
		}
	}

	std::list<Amoeba>::iterator it2 = amoebaList.begin();
	while (it2 != amoebaList.end())
	{
		checkSurroundings(*it2);

		//if the cell is on top of another move it
		while (it2->getAdjacencyList(4) != NULL)
		{
			it2->move();
			checkSurroundings(*it2);
		}

		it2->update();

		checkSurroundings(*it2);

		//if the cell is dead delete it
		if (!it2->isAlive())
		{
			it2 = amoebaList.erase(it2);
		}
		//if the cell has enough energy and space to divide, divide it
		else if (it2->checkForDivide())
		{
			amoebaList.push_back(it2->divide());
			++it2;
		}
		//else just carry onto the next cell
		else
		{
			++it2;
		}
	}
}

void Environment::checkSurroundings(Cell& cell)
{
	checkPositionForPhotoSynthesis(cell);
	checkSurroundingsForOtherCells(cell);
}

//Checks position of cell for feeding then calls update on all cells
void Environment::checkPositionForPhotoSynthesis(Cell& algae)
{
	for (int i = 0; i < SIZE_OF_ENVIRONMENT_X; i++)
	{
		for (int j = 0; j < SIZE_OF_ENVIRONMENT_Y; j++)
		{
			if (i == algae.getPositionX() && j == algae.getPositionY())
			{
				algae.setCurrentTileEnergyValue(lightGrid[i][j]);
			}
		}
	}
}

void Environment::checkSurroundingsForOtherCells(Cell& cell)
{
	cell.setAdjancencyList(0, NULL);
	cell.setAdjancencyList(1, NULL);
	cell.setAdjancencyList(2, NULL);
	cell.setAdjancencyList(3, NULL);
	cell.setAdjancencyList(4, NULL);

	if (locationGrid[cell.getPositionX()][cell.getPositionY() - 1] != NULL)//up
	{
		cell.setAdjancencyList(0, locationGrid[cell.getPositionX()][cell.getPositionY() - 1]);
	}
	if (locationGrid[cell.getPositionX() + 1][cell.getPositionY()] != NULL)//right
	{
		cell.setAdjancencyList(1, locationGrid[cell.getPositionX() + 1][cell.getPositionY()]);
	}
	if (locationGrid[cell.getPositionX()][cell.getPositionY() + 1] != NULL)//down
	{
		cell.setAdjancencyList(2, locationGrid[cell.getPositionX()][cell.getPositionY() + 1]);
	}
	if (locationGrid[cell.getPositionX() - 1][cell.getPositionY()] != NULL)//left
	{
		cell.setAdjancencyList(3, locationGrid[cell.getPositionX() - 1][cell.getPositionY()]);
	}
}

//void Environment::checkSurroundingsForOtherCells(Algae& algae)
//{
//	algae.setAdjancencyList(0, 0);
//	algae.setAdjancencyList(1, 0);
//	algae.setAdjancencyList(2, 0);
//	algae.setAdjancencyList(3, 0);
//	algae.setAdjancencyList(4, 0);
//
//	int currentID = algae.getId();
//	int currentX = algae.getPositionX();
//	int currentY = algae.getPositionY();
//
//	for (std::list<Algae>::iterator it2 = algaeList.begin(); it2 != algaeList.end(); it2++)
//	{
//		int otherID = it2->getId();
//		int otherX = it2->getPositionX();
//		int otherY = it2->getPositionY();
//
//		if (otherX == currentX && otherY == currentY && otherID != currentID)
//		{
//			algae.setAdjancencyList(4, 1);//if current cell is on top of another cell
//		}
//
//		if (otherX == currentX && otherID != currentID)
//		{
//			if (otherY == currentY - 1)
//			{
//				algae.setAdjancencyList(0, 1);//if other cell is above current set to 1
//			}
//			if (otherY == currentY + 1)
//			{
//				algae.setAdjancencyList(2, 1);//if other cell is under current set to 1
//			}
//		}
//		if (otherY == currentY && otherID != currentID)
//		{
//			if (otherX == currentX + 1)
//			{
//				algae.setAdjancencyList(1, 1);//if other cell is right of current set to 1
//			}
//			if (otherX == currentX - 1)
//			{
//				algae.setAdjancencyList(3, 1);//if other cell is left of current set to 1
//			}
//		}
//	}
//}

//Obsolete function
//void Environment::checkIfOnBorder(Cell& cell)
//{
//	int currentID = cell.getId();
//	int currentX = cell.getPositionX();
//	int currentY = cell.getPositionY();
//
//	if (currentX + 1 >= SIZE_OF_ENVIRONMENT_X)
//	{
//		cell.setAdjancencyList(1, 1);
//	}
//	if (currentX - 1 < 0)
//	{
//		cell.setAdjancencyList(3, 1);
//	}
//	if (currentY + 1 >= SIZE_OF_ENVIRONMENT_Y)
//	{
//		cell.setAdjancencyList(2, 1);
//	}
//	if (currentY - 1 < 0)
//	{
//		cell.setAdjancencyList(0, 1);
//	}
//}



