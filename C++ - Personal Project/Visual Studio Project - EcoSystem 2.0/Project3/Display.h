#pragma once

#include "Environment.h"

class Display
{
private:
	Environment environment;
	std::string displayGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];
	std::string displayLocationGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];

public:
	Display();
	~Display();
	void refresh();
	void update(bool mode);
	void show();
	void showLocation();
};

