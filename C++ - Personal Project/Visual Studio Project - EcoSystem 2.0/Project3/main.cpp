#include "Display.h"

//TODO 1: Reduce memory cost
//TODO 2: Teach them to learn

int main(char * argc, int argv[])
{

	Display display;

	bool running = true;
	char input = 'y';

	while (running)
	{
		display.update(0);

		std::cout << "Run another year? Y/n \n";
		std::cin >> input;
		if (input == 'n')
		{
			running = false;
		}
	}
	return 0;
}