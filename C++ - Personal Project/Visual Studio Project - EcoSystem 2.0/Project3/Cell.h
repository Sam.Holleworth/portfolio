#pragma once
#ifndef _CELL_H_
#define _CELL_H_

#include <iostream>
#include "globals.h"

class Cell
{
protected:
	static int nextId;

private:
	int id;
	std::string cellType;
	int parentId;
	int position[2];
	Cell* adjacencyList[5];
	int age;
	int currentTileEnergy;
	int energy;
	bool alive;

public:
	Cell(std::string type);
	Cell(std::string type, int x, int y, int parId);
	~Cell();

	int getId() { return id; }
	std::string getType() { return cellType; }
	int getEnergy() { return energy; }
	int getPositionX() { return position[0]; }
	int getPositionY() { return position[1]; }
	Cell* getAdjacencyList(int index) { return adjacencyList[index]; }
	int getCurrentTileEnergyValue() { return currentTileEnergy; }
	bool isAlive() { return alive; }

	void setEnergy(int newEnergy) { energy = newEnergy; }
	void setCurrentTileEnergyValue(int tileEnergy) { currentTileEnergy = tileEnergy; }
	void setAdjancencyList(int index, Cell* cell) { adjacencyList[index] = cell; }

	virtual void update();
	void displayInfo();
	bool checkForDivide();
	void clamp();
	void move();
	int checkSurroundings();
	void declareLocation();
	void leaveLocation();
	void die();

private:
	void birthday();
	void checkIfDead();

};

#endif _CELL_H_
