#include "Algae.h"

Algae::Algae(std::string type) : Cell(type){}

Algae::Algae(std::string type, int x, int y, int parId) : Cell(type, x, y, parId){}

Algae::~Algae(){}

//Update @Overidden to contain photosynthesize
void Algae::update()
{
	if (isAlive())
	{
		photosynthesize(getCurrentTileEnergyValue());
		Cell::update();
		declareLocation();
	}
}

//Create a new cell in the first available space starting from above and moving clockwise
Algae Algae::divide()
{
	//find the first available space
	int freeSpace = Cell::checkSurroundings();
	int parentId = Cell::getId();

	//spawn a cell in that space
	switch (freeSpace) 
	{
	case 0: 
	{ 
		Algae algae("algae", getPositionX(), getPositionY() - 1, parentId);
		setEnergy(getEnergy() - 15);
		algae.declareLocation();
		return algae;
		break; 
	}
	case 1: 
	{ 
		Algae algae("algae", getPositionX() + 1, getPositionY(), parentId);
		setEnergy(getEnergy() - 15);
		algae.declareLocation();
		return algae;
		break; 
	}
	case 2: 
	{ 
		Algae algae("algae", getPositionX(), getPositionY() + 1, parentId);
		setEnergy(getEnergy() - 15);
		algae.declareLocation();
		return algae;
		break; 
	}
	case 3: 
	{ 
		Algae algae("algae", getPositionX() - 1, getPositionY(), parentId);
		setEnergy(getEnergy() - 15);
		algae.declareLocation();
		return algae;
		break; 
	}
	}
}

void Algae::declareLocation()
{
	Cell::declareLocation();
	locationGrid[getPositionX()][getPositionY()] = this;
}

//Absorb light from current tile
void Algae::photosynthesize(int light)
{
	Cell::setEnergy(Cell::getEnergy() + light);
}