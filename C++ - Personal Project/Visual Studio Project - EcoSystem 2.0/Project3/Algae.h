#ifndef _ALGAE_H_
#define _ALGAE_H_

#include "Cell.h"

class Algae :
	public Cell
{
public:
	Algae(std::string type);
	Algae(std::string type, int x, int y, int parId);
	~Algae();

	void update();
	Algae divide();
	void declareLocation();

private:
	void photosynthesize(int light);
};


#endif _ALGAE_H_