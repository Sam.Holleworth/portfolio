#include <stdlib.h>  
#include "Cell.h"

int Cell::nextId = 0;

//Constructor for spawning a wave of cells with random locations
Cell::Cell(std::string type)
{
	id = nextId++;
	cellType = type;
	parentId = -1;
	position[0] = rand() % SIZE_OF_ENVIRONMENT_X;
	position[1] = rand() % SIZE_OF_ENVIRONMENT_Y;
	for (int i = 0; i < 5; i++)
	{
		adjacencyList[i] = NULL;
	}
	age = 0;
	energy = 100;
	alive = true;
}

//Constructor for cell division
Cell::Cell(std::string type, int x, int y, int parId)
{
	id = nextId++;
	cellType = type;
	parentId = parId;
	position[0] = x;
	position[1] = y;
	age = 0;
	energy = 10;
	for (int i = 0; i < 5; i++)
	{
		adjacencyList[i] = NULL;
	}
}

Cell::~Cell()
{
}

void Cell::update()
{
	birthday();
	move();
}

/*
Function uses a random number to determine the direction moved each update
if the number is over 6 move over the x axis, if under 6 move over the y
if the number is odd add 1 to x or y, if number is even minus 1
*/
void Cell::move()
{
	int randomNumber = rand() % 12 +  1;

	//move on x
	if (randomNumber > 6)
	{
		if (randomNumber % 2 == 1 && adjacencyList[1] == NULL)//move right if not blocked
		{
			leaveLocation();
			position[0] += 1;
		}
		else if(adjacencyList[3] == NULL)//move left if not blocked
		{
			leaveLocation();
			position[0] -= 1;
		}
	}

	//move on y
	else
	{
		if (randomNumber % 2 == 1 && adjacencyList[0] == NULL)//move up if not blocked
		{
			leaveLocation();
			position[1] -= 1;
		}
		else if (adjacencyList[2] == NULL)//move down if not blocked
		{
			leaveLocation();
			position[1] += 1;
		}
	}
	clamp();
	declareLocation();
}

void Cell::clamp()
{
	if (position[0] < 0) 
	{ 
		position[0] = 0; 
	}
	else if (position[0] >= SIZE_OF_ENVIRONMENT_X) 
	{ 
		position[0] = SIZE_OF_ENVIRONMENT_X - 1; 
	}
	if (position[1] < 0) 
	{
		position[1] = 0; 
	}
	else if (position[1] >= SIZE_OF_ENVIRONMENT_Y) 
	{ 
		position[1] = SIZE_OF_ENVIRONMENT_Y - 1; 
	}
}


//Age a cell then check if it expires
void Cell::birthday()
{
	age++;
	energy -= 5;
	checkIfDead();
}

//If the cell is too old or starving kill it
void Cell::checkIfDead()
{
	if (age >= MAX_AGE)
	{
		die();
	}
	else if (energy <= 0)
	{
		die();
	}
}

//Kill cell
void Cell::die()
{
	alive = false;
	leaveLocation();
	Cell::~Cell();
}

//If energy is high enough for division return true
bool Cell::checkForDivide()
{
	int check = checkSurroundings();

	if (energy >= REPRODUCTION_ENERGY && check != -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Cell::checkSurroundings()
{
	for (int i = 0; i < 4; i++)
	{
		if (adjacencyList[i] == NULL)
		{
			return i;
		}
	}
	return -1;
}

void Cell::declareLocation()
{

}

void Cell::leaveLocation()
{
	locationGrid[position[0]][position[1]] = NULL;
}

//Print info to console
void Cell::displayInfo()
{
	std::cout << "ID: " << id << "\n";
	std::cout << "Parent ID: " << parentId << "\n";
	std::cout << "Position: [" << position[0] << "," << position[1] <<  "]\n";
	std::cout << "Age: " << age << "\n";
	std::cout << "Energy: " << energy << "\n";
	std::cout << "Adjacency List: [" << adjacencyList[0] << ", " << adjacencyList[1] << ", " << adjacencyList[2] << ", " << adjacencyList[3] << ", " << adjacencyList[4] << "]";
	std::cout << "\n\n";
}
