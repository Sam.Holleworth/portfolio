#include "Amoeba.h"

Amoeba::Amoeba(std::string type) : Cell(type) {}

Amoeba::Amoeba(std::string type, int x, int y, int parId) : Cell(type, x, y, parId) {}

Amoeba::~Amoeba(){}

void Amoeba::update()
{
	if (isAlive())
	{
		Cell::update();
		declareLocation();
		eat();
	}
}

//Create a new cell in the first available space starting from above and moving clockwise
Amoeba Amoeba::divide()
{
	//find the first available space
	int freeSpace = Cell::checkSurroundings();
	int parentId = Cell::getId();

	//spawn a cell in that space
	switch (freeSpace)
	{
	case 0:
	{
		Amoeba amoeba("amoeba", getPositionX(), getPositionY() - 1, parentId);
		setEnergy(getEnergy() - 15);
		amoeba.declareLocation();
		return amoeba;
		break;
	}
	case 1:
	{
		Amoeba amoeba("amoeba", getPositionX() + 1, getPositionY(), parentId);
		setEnergy(getEnergy() - 15);
		amoeba.declareLocation();
		return amoeba;
		break;
	}
	case 2:
	{
		Amoeba amoeba("amoeba", getPositionX(), getPositionY() + 1, parentId);
		setEnergy(getEnergy() - 15);
		amoeba.declareLocation();
		return amoeba;
		break;
	}
	case 3:
	{
		Amoeba amoeba("amoeba", getPositionX() - 1, getPositionY(), parentId);
		setEnergy(getEnergy() - 15);
		amoeba.declareLocation();
		return amoeba;
		break;
	}
	}
}

void Amoeba::eat()
{
	for (int i = 0; i < 4; i++)
	{
		Cell* adjacentCell = getAdjacencyList(i);
		if (adjacentCell->getType() == "algae")
		{
			setEnergy(adjacentCell->getEnergy() / 2);
			adjacentCell->die();
			adjacentCell = NULL;
			return;
		}
		adjacentCell = NULL;
	}
}

void Amoeba::declareLocation()
{
	locationGrid[getPositionX()][getPositionY()] = this;
}
