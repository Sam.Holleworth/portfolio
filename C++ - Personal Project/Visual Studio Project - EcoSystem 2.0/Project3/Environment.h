#pragma once

#include <list>
#include "Cell.h"
#include "Amoeba.h"
#include "Algae.h"

class Environment
{
private:
	std::list<Algae> algaeList;
	std::list<Amoeba> amoebaList;
	int lightGrid[SIZE_OF_ENVIRONMENT_X][SIZE_OF_ENVIRONMENT_Y];


public:
	Environment();
	~Environment();

	std::list<Algae> getAlgaeList() { return algaeList; }
	std::list<Amoeba> getAmeobaList() { return amoebaList; }

	void intializeGridValues();
	void spawnAlgae(int numberOfRequestedAlgae);
	void spawnAmoeba(int numberOfRequestedAmoeba);
	void displayCellInfo(char mode);
	void run();

private:
	void checkSurroundings(Cell& cell);
	void checkPositionForPhotoSynthesis(Cell& algae);
	void checkSurroundingsForOtherCells(Cell& cell);
	void checkIfOnBorder(Cell& cell);
};

