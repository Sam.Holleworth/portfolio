#ifndef ENV_H
#define ENV_H

struct EnvironmentNode
{
  int id;
  int x;
  int y;
  int light;
};

#endif
