#include "environment.h"
#include "definitions.h"

#include <cmath>
#include "Sun.h"

Sun::Sun(int n, int m, int l) // default constructor
{
  x = n;
  y = m;
  light = l;
};

// getters
int Sun::getX() {return x;}
int Sun::getY() {return y;}
int Sun::getLight() {return light;}

//give light to each environment node
void Sun::shine(EnvironmentNode n[])
{
  for(int i = 0; i < sizeOfEnvironment; i++)
  {
    //find suns x & y position
    int tempSunX = Sun::getX();
    int tempSunY = Sun::getY();
    //find current nodes x & y position
    int tempEnvX = n[i].x;
    int tempEnvY = n[i].y;

    int distanceX, distanceY;

    //find distance difference between sun and node, and then remove the sign
    distanceX = sqrt((tempSunX-tempEnvX)*(tempSunX-tempEnvX));
    distanceY = sqrt((tempSunY-tempEnvY)*(tempSunY-tempEnvY));

    //allocate light value
    n[i].light = lightPower-distanceX-distanceY;

    if(n[i].light < 0)
    {
      n[i].light = 0;
    }
  }
}
