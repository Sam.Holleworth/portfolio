#include "Algae.h"

Algae::Algae(int n, int m, int size, int chloro)
: Cell(n, m, size)
{
  Algae::setChloroSize(chloro);

	Algae::nextId++;
	Algae::setId(nextId);
}

Algae::~Algae(){}

int Algae::nextId = 0;

int Algae::getChloroSize(){return chloroSize;}

void Algae::setChloroSize(int chloro){chloroSize = chloro;}

//Add any light at current coordinates to energy
void Algae::photosynthesize(EnvironmentNode nodeArray[])
{
  for(int i=0; i < sizeOfEnvironment; i++)
  {
    //If cells coordinates equal a environodes coordinate eat the light
    if(getX() == nodeArray[i].x && getY() == nodeArray[i].y)
    {
      setEnergy(getEnergy()+nodeArray[i].light);
    }
    //if the cell has excess energy add it to reproduction energy
    if(getEnergy() >= energyLimit)
    {
      setReproductionEnergy(getReproductionEnergy() + (getEnergy()-energyLimit));
      setEnergy(energyLimit);
    }
  }
}

Algae Algae::reproduce()
{
	int newX, newY, newSize, newChloroSize, newId;
	newX = getX();
	newY = getY();
	newSize = rand() % 70 + 30;
	newChloroSize = rand() % 10 + 1;

	Algae newAlgae(newX, newY, newSize, newChloroSize);

	setReproductionEnergy(0);

	return newAlgae;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////ALGAE LINKED LIST BEGINS////////////////////////////////////////////////////////////////////////



#define aL algaeList

algaeNode *aL::getHead() { return head; }

aL::algaeList()
{
	head = NULL;
	tail = NULL;
}

void aL::createNode(Algae value)
{
	algaeNode *temp = new algaeNode(-1, -1, -1, -1);

	temp->data = value;
	temp->next = NULL;

	if (head == NULL)
	{
		head = temp;
		tail = temp;
		temp = NULL;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}
}

void aL::displayAlgaeNodeArray()
{
	algaeNode *temp = new algaeNode(-1, -1, -1, -1);

	while (temp != NULL)
	{

		int id = temp->data.getId();
		int x = temp->data.getX();
		int y = temp->data.getY();
		int size = temp->data.getSize();
		int age = temp->data.getAge();
		int vel = temp->data.getVel();
		int energy = temp->data.getEnergy();
		int reproductionEnergy = temp->data.getReproductionEnergy();
		int chloroSize = temp->data.getChloroSize();


		printf("Algae ID: %i\nCoordinates (%i, %i)\nSize: %i\nAge: %i\nVel: %i\nEnergy: %i\nReproduction Energy: %i\nChloroSize: %i\n\n\n", id, x, y, size, age, vel, energy, reproductionEnergy, chloroSize);
		temp = temp->next;
	}
}

void aL::addAlgaeNodeFront(Algae new_algae)
{
	algaeNode *temp = new algaeNode(-1, -1, -1, -1);
	temp->data = new_algae;
	temp->next = head;
	temp = head;
}

void aL::addAlgaeNodeMiddle(Algae new_algae, algaeNode *prev_node)
{
	if (prev_node == NULL)
	{
		std::cout << "Previous node cannot be NULL.";
	}

	algaeNode *temp = new algaeNode(-1, -1, -1, -1);

	temp->data = new_algae;

	temp->next = prev_node->next;

	prev_node->next = temp;
}
