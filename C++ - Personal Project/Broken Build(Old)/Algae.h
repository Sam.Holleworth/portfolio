#ifndef ALGAE_H
#define ALGAE_H

#include "Cell.h"



class Algae: public Cell
{
  protected:
    int chloroSize;

  public:
		static int nextId;

    Algae(int,int,int,int);
    ~Algae();

    int getChloroSize();

    void setChloroSize(int i);

    void photosynthesize(EnvironmentNode nodeArray[]);
		Algae reproduce();
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////ALGAE LINKED LIST BEGINS/////////////////////////////////////////////////////////////////


struct algaeNode
{
	algaeNode(int n, int m, int size, int chloro) : data(n, m, size, chloro) {}
	Algae data;
	algaeNode *next;
};

class algaeList
{
protected:
	algaeNode *head, *tail;

public:
	algaeNode *getHead();

	algaeList();
	void createNode(Algae value);
	void displayAlgaeNodeArray();
	void addAlgaeNodeFront(Algae new_algae);
	void addAlgaeNodeMiddle(Algae new_algae, algaeNode *prev_node);
};



#endif
