#ifndef sun_H
#define sun_H

class Sun
{
  protected:
    int x;
    int y;
    int light;

  public:
    Sun(int, int, int);

    int getY();
    int getX();
    int getLight();

    void shine(EnvironmentNode n[]);
};

#endif
