#include "Cell.h"


Cell::Cell(int n, int m, int size)
{
  setX(n);
  setY(m);
  setSize(size);
  setAge(0);
  setVel(1);
  setEnergy(5);
  setReproductionEnergy(0);
	setId(0);
}

//getters
int Cell::getX() {return x;}
int Cell::getY() {return y;}
int Cell::getSize() {return size;}
int Cell::getAge() {return age;}
int Cell::getVel() {return vel;}
int Cell::getEnergy() {return energy;}
int Cell::getReproductionEnergy(){return reproductionEnergy;}
int Cell::getId(){return id;}

//setter
void Cell::setX(int i) {x = i;}
void Cell::setY(int i) {y = i;}
void Cell::setSize(int i) {size = i;}
void Cell::setAge(int i) {age = i;}
void Cell::setVel(int i) {vel = i;}
void Cell::setEnergy(int i) {energy = i;}
void Cell::setReproductionEnergy(int i) {reproductionEnergy = i;}
void Cell::setId(int i) {id = i;}

//Add 1 to cells size if it has enough energy
void Cell::grow()
{
  if(energy > 10)
  {
    Cell::setSize(Cell::getSize()+1);
  }
}

//Add 1 to cells age
void Cell::incrementAge()
{
  Cell::setAge(Cell::getAge()+1);
}

//move a cell in a random direction
void Cell::move()
{
  //assign some randowm numbers
  int n = rand();
  int m = rand();

  //get x and y position of cell
  int x = Cell::getX();
  int y = Cell::getY();
  int vel = Cell::getVel();

  //if number is odd move on x axis
  if(n%2 == 1)
  {
    //if number is odd move right
    if(m%2 == 1)
    {
      //if cell is not going to move outside of environent move it
      if(x+vel > envX-vel)
      {
        Cell::setX(envX);
      }
      else
      {
        Cell::setX(x+vel);
      }
    }
    //if number is even move left
    else
    {
      if(x-vel < 0)
      {
        Cell::setX(0);
      }
      else
      {
        Cell::setX(x-vel);
      }
    }
  }

  //if number is even move on y axis
  else
  {
    //if number is odd move down
    if(m%2 == 1)
    {
      //if cell is not going to move outside of environment move it
      if(y+vel > envY)
      {
        Cell::setY(envY);
      }
      else
      {
        Cell::setY(y+vel);
      }
    }
    //if number is even move up
    else
    {
      if(y < 0)
      {
        Cell::setY(0);
      }
      else
      {
        Cell::setY(y-vel);
      }
    }
  }
}
