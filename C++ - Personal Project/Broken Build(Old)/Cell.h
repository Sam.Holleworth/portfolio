#ifndef CELL_H
#define CELL_H

#include "environment.h"
#include "definitions.h"

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <stdio.h>

class Cell {
  protected:
    int x;
    int y;
    int size;
    int age;
    int vel;
    int energy;
    int reproductionEnergy;
    int id;

  public:
    Cell(int,int,int);

    int getX();
    int getY();
    int getSize();
    int getAge();
    int getVel();
    int getEnergy();
    int getReproductionEnergy();
    int getId();

    void setX(int i);
    void setY(int i);
    void setSize(int i);
    void setAge(int i);
    void setVel(int i);
    void setEnergy(int i);
    void setReproductionEnergy(int i);
    void setId(int i);

    void grow();
    void incrementAge();
    void move();
};

#endif
