#include <iostream>
#include <cmath>
#include <Windows.h>

#include "environment.h"
#include "Sun.h"
#include "definitions.h"
#include "Algae.h"


EnvironmentNode nodeArray[sizeOfEnvironment + 1];
algaeList livingAlgaeCells;

//create a requested number of new algae cells
void createAlgae(int requestedAlgae)
{

  for(int i = 0; i < requestedAlgae; i++)
  {
    int newX, newY, newSize, newId, newChloroSize;
    //give cell random size between 30 and 100
    newSize = rand() % 70+30;

    //set flag to false for coordinate scanning
    bool clear = false;

    //assign a set of unoccupied coordinates to a cell
    while(clear != true)
    {
      clear = true;

      //give x and y a random value within the environment matrix
      newX = rand() % envX;
      newY = rand() % envY;

      //temporary node for scanning existing cells
      algaeNode *temp = new algaeNode(-1, -1, -1, -1);

      temp = livingAlgaeCells.getHead();

      //cycle through nodes, if the cycle ends clear is true and the cell can exist
      while(temp != NULL && clear != false)
      {
        //if temp is a node which matches newX and newY, clear = false and entire loop repeats
        if(temp->data.getX() == newX && temp->data.getY() == newY)
        {
          clear = false;
        }
        temp = temp->next;
      }
    }

    //assign random chloro size 1-10
    newChloroSize = rand() %10 + 1;

    //use all assigned new values to create an algae object
    Algae newAlgae(newX, newY, newSize, newChloroSize);

    livingAlgaeCells.createNode(newAlgae);

  }
}

void advanceAllCells()
{
	algaeNode *temp = new algaeNode(-1, -1, -1, -1);
	temp = livingAlgaeCells.getHead();
	while (temp != NULL)
	{
		temp->data.move();
		temp->data.photosynthesize(nodeArray);
		if (temp->data.getReproductionEnergy() >= reproductionLimit)
		{
			livingAlgaeCells.createNode(temp->data.reproduce());
		}
		temp = temp->next;
	}
}

//void initstatics()
//{
//	createalgae(1);
//	algaenode *temp = new algaenode(-1, -1, -1, -1, -1);
//	temp = livingalgaecells.gethead();
//	while (temp != null)
//	{
//		temp->data.initchlor();
//		temp->data.~algae();
//		temp = temp->next;
//	}
//}

int main()
{

  //create the sun
  Sun Sol(sunPosX, 0, lightPower);

  int counter = 0;

  //initiate environment grid
  for(int i=0; i<envX; i++)
  {
    for(int j=0; j<envY; j++)
    {
      nodeArray[counter].x = i;
      nodeArray[counter].y = j;
      nodeArray[counter].id = 1 + counter;
      counter++;
    }
  }

  //give all nodes a light value
  Sol.shine(nodeArray);

  // //print node location and light value
  // for(int i=0; i<sizeOfEnvironment; i++)
  // {
  //   std::cout << "Node location (" << nodeArray[i].x << "," << nodeArray[i].y << ") "
  //   << "Light " << ": " << nodeArray[i].light << "\n";
  // }
  createAlgae(3);

	bool running = true;
	
	while (running != false)
	{
		advanceAllCells();
		livingAlgaeCells.displayAlgaeNodeArray();
		Sleep(2000);
	}
}
