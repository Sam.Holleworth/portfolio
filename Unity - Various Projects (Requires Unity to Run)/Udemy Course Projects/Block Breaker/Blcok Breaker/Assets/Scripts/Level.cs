﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{

    [SerializeField] int breakableBlocks; //Serialized for debugging

    SceneLoader sceneLoader;

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    public void CountBlocks()
    {
        breakableBlocks++;
    }

    public void BreakBlock()
    {
        breakableBlocks--;
        CheckIfLevelWon();
    }

    public void CheckIfLevelWon()
    {
        if(breakableBlocks == 0)
        {
            sceneLoader.LoadNextScene();
        }
    }

}
